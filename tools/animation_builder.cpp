#include "Animation.hpp"

#include <cereal/archives/xml.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/program_options.hpp>

#define FILE_EXT ".xml"

using namespace boost::program_options;

auto print_help() -> void
{
    std::cout << "Usage: animation_builder -b -n <num frames> -a <num animations> -o <output filename>" << std::endl;
}

auto prompt(const std::string& msg, std::string& dest)
{
    std::cout << msg << ": ";
    std::cin >> dest;
}

auto parse_csv_ints(const std::string& row) -> std::vector<int>
{
    std::vector<int> out;
    std::stringstream ss(row);
    while (ss.good())
    {
        std::string substr;
        std::getline(ss, substr, ',');
        out.push_back(std::atoi(substr.c_str()));
    }

    return out;
}

auto build_animation(int num_animations, int num_frames, const std::string& filename) -> void
{
    AnimationFile file;

    file.animations.reserve(num_animations + 1);
    file.frames.reserve(num_frames);

    AnimationData defaultAnim;
    defaultAnim.frameIndexes.push_back(0);
    defaultAnim.name = "default";
    defaultAnim.fps = 0;

    file.animations.push_back(std::move(defaultAnim));

    for (int i = 0; i < num_frames; ++i)
    {

        std::string xyzw;
        std::cout << "Add frame " << i << " in the format x,y,w,h: ";
        std::cin >> xyzw;
        const auto parts = parse_csv_ints(xyzw);
        file.frames.push_back({parts[0], parts[1], parts[2], parts[3]});
    }

    for (int i = 0; i < num_animations; ++i)
    {
        AnimationData anim;

        std::string name;
        double fps;
        std::string frameCSV;

        std::cout << "enter animation " << i << " name: ";
        std::cin >> name;

        std::cout << "enter animation " << i << " fps: ";
        std::cin >> fps;

        std::cout << "enter frame list for animation " << i << " as a csv row: ";
        std::cin >> frameCSV;

        anim.name = name;
        anim.fps = fps;

        const auto parts{parse_csv_ints(frameCSV)};
        anim.frameIndexes.insert(anim.frameIndexes.begin(), parts.begin(), parts.end());

        file.animations.push_back(std::move(anim));
    }

    std::ofstream out(filename + FILE_EXT);
    cereal::XMLOutputArchive archive(out);
    archive(file);
}

auto main(int argc, char** argv) -> int
{
    try
    {
        options_description desc{"Options"};
        desc.add_options()
            ("build,b", "Build an animation")
            ("frames,f", value<int>(), "Amount of frames to create")
            ("animations,a", value<int>(), "Amount of animations to create")
            ("output,o", value<std::string>(), "Output file name")
            ("help,h", "Help screen");

        variables_map vm;
        store(parse_command_line(argc, argv, desc), vm);
        notify(vm);

        if (vm.count("help"))
        {
            print_help();
        }
        else if (vm.count("build"))
        {
            build_animation(vm["animations"].as<int>(), vm["frames"].as<int>(), vm["output"].as<std::string>());
        }
        else
        {
            print_help();
        }
    }
    catch (const error& ex)
    {
        std::cerr << ex.what() << std::endl;
        return 1;
    }

    return 0;
}