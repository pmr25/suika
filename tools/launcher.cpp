#include <cstdlib>
#include <filesystem>
#include <iostream>

namespace fs = std::filesystem;

auto main(int argc, char** argv) -> int
{
    const char* env_root = std::getenv("APPDIR");
    if (!env_root)
    {
        std::cout << "error" << std::endl;
        return 1;
    }

    const char* env_home = std::getenv("HOME");
    if (!env_home)
    {
        std::cout << "error" << std::endl;
        return 1;
    }

    const fs::path root(env_root);
    const fs::path home(env_home);
    const fs::path appdata(home / ".local/share/suikagame");
    const fs::path bin(root / "usr/bin");

    const fs::path suika_exe(bin / "suika");
    const fs::path housekeeper_exe(bin / "suika_housekeeper");
    const fs::path logs_dir(appdata / "logs");

    std::system(suika_exe.c_str());
    std::system(std::string(housekeeper_exe.string() + " " + logs_dir.string()).c_str());

    return 0;
}