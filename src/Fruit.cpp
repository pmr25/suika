#include "Fruit.hpp"
#include "FruitList.hpp"
#include "SfxPlayer.hpp"

#include <glm/gtx/string_cast.hpp>

#define SOUND_VELOCITY_THR 250.0F

void Fruit::init()
{
    mSprite.setTexture(FruitList::getInstance().getTexture(mLevel));
}

void Fruit::flag()
{
    mSprite.setColorMod({0.8F, 0.1F, 0.1F});
}

void Fruit::addPhysicsObject(const glm::vec2& initialVel)
{
    const auto center{mSprite.getCenter()};
    const auto& dimension{mSprite.getDimensions()};
    const auto physId{PhysicsWorld::getInstance().createPhysicsObject(1.0F, true)};
    auto physObj = PhysicsWorld::getInstance().getPhysicsObject(physId);
    assert(physObj);
    physObj->createCircle(center.x, center.y, dimension.x);
    physObj->setVelocity(initialVel);
    setPhysicsObject(physId);
}

void Fruit::setPhysicsObject(int physId)
{
    mPhysObjId = physId;
    auto physObj = PhysicsWorld::getInstance().getPhysicsObject(physId);
    assert(physObj);

    physObj->setListener(this);

    const auto physPos{physObj->getPosition()};
    mSprite.setCenter({physPos.x, physPos.y});
}

void Fruit::update()
{
    if (mPhysObjId == -1)
    {
        return;
    }

    auto physObject{PhysicsWorld::getInstance().getPhysicsObject(mPhysObjId)};
    if (physObject == nullptr)
    {
        return;
    }

    const auto dim{mSprite.getDimensions()};
    const auto physPos{physObject->getPosition()};
    mSprite.setPosition({physPos.x - dim.x / 2, physPos.y - dim.y / 2});
    mSprite.setTheta(physObject->getTheta());

    mLastVelocity = getVelocity();

    mIsMuted = false;
}

auto Fruit::getSprite() -> Sprite&
{
    return mSprite;
}

void Fruit::notifyCollision(CollisionListener* other)
{
    mIsActive = true;

    if (other == nullptr)
    {
        playSound("ping");
        return;
    }

    auto otherFruit{dynamic_cast<Fruit*>(other)};
    assert(otherFruit);

    if (fused > -1 || otherFruit->fused > -1)
    {
        playSound("thud");
        return;
    }

    if (otherFruit->mLevel == mLevel)
    {
        otherFruit->tempMuteSfx();

        if (mLevel + 1 > FruitList::getInstance().getMaxLevel())
        {
            playSound("thud");
            return;
        }

        otherFruit->fused = index;
        fused = otherFruit->index;
        assert(fused != index);
    }
    else
    {
        playSound("thud");
    }
}

auto Fruit::getLevel() const -> int
{
    return mLevel;
}

void Fruit::setIndex(int idx)
{
    index = idx;
}

auto Fruit::getIndex() const -> int
{
    return index;
}

auto Fruit::getFusedIndex() const -> int
{
    return fused;
}

auto Fruit::isActive() const -> bool
{
    return mIsActive;
}

auto Fruit::isStationary() const -> bool
{
    return isActive() && glm::length(getVelocity()) < 1e-3;
}

auto Fruit::getDiameter() const -> float
{
    return float(FruitList::getInstance().getDiameter(mLevel));
}

auto Fruit::getVelocity() const -> glm::vec2
{
    const auto physObject{PhysicsWorld::getInstance().getPhysicsObject(mPhysObjId)};
    return physObject->getVelocity();
}

auto Fruit::getLastVelocity() const -> const glm::vec2&
{
    return mLastVelocity;
}

void Fruit::tempMuteSfx()
{
    mIsMuted = true;
}

void Fruit::playSound(const std::string& key) const
{
    if (mIsMuted)
    {
        return;
    }

    if (glm::length(getVelocity()) > SOUND_VELOCITY_THR)
    {
        const auto pitch{
            1.5F - 0.2F * (static_cast<float>(mLevel) / static_cast<float>(FruitList::getInstance().getMaxLevel()))};
        const auto gain{
            0.25F + 0.5F * (static_cast<float>(mLevel) / static_cast<float>(FruitList::getInstance().getMaxLevel()))};
        SfxPlayer::getInstance().queueSample(key, gain, pitch);
    }
}
