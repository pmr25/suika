#include "AnimationPlayer.hpp"
#include "Animation.hpp"
#include <cassert>

auto AnimationPlayer::setCurrentAnimation(const std::string& key) -> std::optional<AnimationPlayerError>
{
    if (!mAnimation)
    {
        return {AnimationPlayerError::AnimationNotSet};
    }

    const int nextAnimation{mAnimation->getAnimationID(key)};
    if (mAnimID == nextAnimation)
    {
        return {AnimationPlayerError::AnimationAlreadySet};
    }

    mAnimID = nextAnimation;
    if (mAnimID < 0)
    {
        return {AnimationPlayerError::AnimationNotFound};
    }

    mTimer = mAnimation->getTickTimer(mAnimID);

    return std::nullopt;
}

auto AnimationPlayer::update() -> void
{
    if (!isRunning())
    {
        return;
    }

    const auto& frames{mAnimation->getFrameIndexes(mAnimID)};

    if (mTimer.tick())
    {
        mTimer.reset();
        assert(mPosition >= 0);
        if (uint64_t(mPosition) >= frames.size() - 1)
        {
            mPosition = mShouldLoop ? 0 : mPosition;
            mIsRunning = mShouldLoop;
        }
        else
        {
            ++mPosition;
        }
    }
}

auto AnimationPlayer::getCurrentFrame() const -> SDL_Rect
{
    if (!mAnimation || mAnimID == -1)
    {
        return {0, 0, 0, 0};
    }
    const auto indexes{mAnimation->getFrameIndexes(mAnimID)};
    assert(mPosition <= static_cast<int>(indexes.size()));
    return mAnimation->getFrame(indexes[mPosition]);
}

auto AnimationPlayer::isReady() const -> bool
{
    return mAnimation && mAnimID >= 0;
}

auto AnimationPlayer::isRunning() const -> bool
{
    return isReady() && mIsRunning;
}

auto AnimationPlayer::setShouldLoop(bool value) -> void
{
    mShouldLoop = value;
}

auto AnimationPlayer::play() -> void
{
    mIsRunning = true;
}

auto AnimationPlayer::setAnimationProvider(const std::shared_ptr<Animation>& provider) -> void
{
    mAnimation = provider;
    setCurrentAnimation("default");
}
auto AnimationPlayer::getCurrentFrameVector() const -> const glm::vec4
{
    const auto frame = getCurrentFrame();
    return {frame.x, frame.y, frame.w, frame.h};
}
