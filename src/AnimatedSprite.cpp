#include "AnimatedSprite.hpp"

#include <utility>
auto AnimatedSprite::setCurrentAnimation(const std::string& key) -> std::optional<AnimatedSpriteError>
{
    const auto setAnimErr = mAnimPlayer.setCurrentAnimation(key);
    if (setAnimErr == AnimationPlayerError::AnimationNotFound)
    {
        return {AnimatedSpriteError::CannotSetAnimation};
    }
    else if (setAnimErr == AnimationPlayerError::AnimationAlreadySet)
    {
        return std::nullopt;
    }

    mAnimPlayer.play();

    return std::nullopt;
}

void AnimatedSprite::update()
{
    mAnimPlayer.update();
    if (!mSprite.getTexture())
    {
        return;
    }
    mSprite.setFrame(mAnimPlayer.getCurrentFrameVector());
}

auto AnimatedSprite::setAnimationProvider(std::shared_ptr<Animation>& ani) -> void
{
    mAnimPlayer.setAnimationProvider(ani);
}

auto AnimatedSprite::getSprite() -> Sprite&
{
    return mSprite;
}
auto AnimatedSprite::getAnimPlayer() -> AnimationPlayer&
{
    return mAnimPlayer;
}
