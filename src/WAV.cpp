#include "WAV.hpp"

#include <array>
#include <cstring>
#include <fstream>
#include <iostream>

#define IS_BIG_ENDIAN (!*(unsigned char*)&(uint16_t){1})
#ifndef IS_BIG_ENDIAN
#define LTOB16(x) (x = (((x) & 0xFF) << 8) | (((x) >> 8) & 0xFF))
#define LTOB32(x) (x = __builtin_bswap32(x))
#endif

static auto getRIFFHeaderFormat(RIFFHeader* header) -> std::array<std::pair<void*, uint8_t>, 13>
{
    return {
        {
            {static_cast<void*>(&header->riff), 4},
            {static_cast<void*>(&header->file_size), 4},
            {static_cast<void*>(&header->file_type), 4},
            {static_cast<void*>(&header->fmt), 4},
            {static_cast<void*>(&header->format_length), 4},
            {static_cast<void*>(&header->format_type), 2},
            {static_cast<void*>(&header->num_channels), 2},
            {static_cast<void*>(&header->sample_rate), 4},
            {static_cast<void*>(&header->bit_rate), 4},
            {static_cast<void*>(&header->sample_channel_bytes), 2},
            {static_cast<void*>(&header->bits_per_sample), 2},
            {static_cast<void*>(&header->data), 4},
            {static_cast<void*>(&header->data_size), 4},
        },
    };
}

void WAV::load(const std::string& fn)
{
    std::ifstream in(fn, std::ios::binary);
    if (!in)
    {
        return;
    }

    load(in);
}

void WAV::load(std::istream& in)
{
    const auto format{getRIFFHeaderFormat(&mHeader)};
    for (const auto& ptrSizePair : format)
    {
        in.read(static_cast<char*>(ptrSizePair.first), ptrSizePair.second);
        if (in.fail())
        {
            return;
        }
    }

#ifndef IS_BIG_ENDIAN
    LTOB32(mHeader.file_size);
    LTOB32(mHeader.format_length);
    LTOB16(mHeader.format_type);
    LTOB16(mHeader.num_channels);
    LTOB32(mHeader.sample_rate);
    LTOB32(mHeader.bit_rate);
    LTOB16(mHeader.sample_channel_bytes);
    LTOB16(mHeader.bits_per_sample);
    LTOB32(mHeader.data_size);
#endif

    if (!validateHeader())
    {
        return;
    }

    const int data_size{mHeader.data_size};

    mPCM.resize(data_size);
    in.read(reinterpret_cast<char*>(mPCM.data()), data_size);
    mPCM.resize(in.gcount());
}

auto WAV::validateHeader() const -> bool
{
    return strncmp(mHeader.riff, "RIFF", 4) == 0 && strncmp(mHeader.file_type, "WAVE", 4) == 0 &&
           strncmp(mHeader.fmt, "fmt", 3) == 0 && strncmp(mHeader.data, "data", 4) == 0 && mHeader.format_type == 1 &&
           mHeader.num_channels == 2;
}

auto WAV::getFormat() const -> PCMFormat
{
    return static_cast<PCMFormat>(mHeader.sample_channel_bytes);
}

auto WAV::getSize() const -> int32_t
{
    return mHeader.data_size;
}
auto WAV::getRate() const -> int32_t
{
    return mHeader.sample_rate;
}
auto WAV::getData() const -> const uint8_t*
{
    return &mPCM[0];
}
