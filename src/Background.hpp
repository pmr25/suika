#ifndef BACKGROUND_HPP
#define BACKGROUND_HPP

#include "Sprite.hpp"

#include <vector>

class TextureImage;

class Background
{
public:
    Background()
    {
        init();
    }

    void init();
    void update();
    [[nodiscard]] auto getSprites() const -> const std::vector<Sprite>&;

private:
    void addBubble();
    void resetBubble(size_t);

    std::vector<Sprite> mSprites;
    std::vector<size_t> mBubbles;
    std::shared_ptr<Texture> mBubbleTex;
};

#endif // BACKGROUND_HPP
