#ifndef ASSETS_HPP
#define ASSETS_HPP

#include <cstdint>
#include <string>
#include <vector>

struct SDL_RWops;

class AssetFileHandle
{
public:
    explicit AssetFileHandle(const std::vector<uint8_t>&& data)
        : mData(data)
    {
        initOps();
    }
    ~AssetFileHandle()
    {
        destroyOps();
    }
    AssetFileHandle(const AssetFileHandle& other) = delete;
    auto operator=(const AssetFileHandle& other) -> AssetFileHandle& = delete;
    AssetFileHandle(AssetFileHandle&& other) noexcept
    {
        mData = std::move(other.mData);
        mOps = other.mOps;
        other.mOps = nullptr;
    }
    auto operator=(AssetFileHandle&& other) noexcept -> AssetFileHandle&
    {
        if (this == &other)
        {
            return *this;
        }

        mData = std::move(other.mData);
        mOps = other.mOps;
        other.mOps = nullptr;

        return *this;
    }

    void initOps();
    void destroyOps();
    [[nodiscard]] auto getOps() -> SDL_RWops*;
    auto getData() -> const std::vector<uint8_t>&;

private:
    std::vector<uint8_t> mData;
    SDL_RWops* mOps{nullptr};
};

class Assets
{
public:
    static auto getInstance() -> const Assets&;

    ~Assets() = default;

    [[nodiscard]] auto getSize() const -> int;

    [[nodiscard]] auto getFile(const std::string& fn) const -> AssetFileHandle;

private:
    Assets() = default;

    [[nodiscard]] auto openFile(const std::string&) const -> std::vector<uint8_t>;
};

#endif // ASSETS_HPP
