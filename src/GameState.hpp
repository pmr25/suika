#ifndef GAMESTATE_HPP
#define GAMESTATE_HPP

#include <string>
#include <utility>

union SDL_Event;

enum class GameStatePhase
{
    IDLE,
    ENTER,
    MAIN,
    LEAVE,
};

class GameState
{
public:
    virtual ~GameState() = default;
    virtual void init() = 0;
    virtual void handleEvent(SDL_Event*) = 0;
    virtual void update() = 0;
    virtual void draw() = 0;
    virtual auto isReady() const -> bool = 0;
    virtual void setReady(bool) = 0;
    [[nodiscard]] virtual auto getCurrentPhase() const -> std::pair<GameStatePhase, std::string> = 0;
    virtual void setPhase(GameStatePhase, bool) = 0;
    virtual void setNextState(const std::string&) = 0;
};

#endif // GAMESTATE_HPP
