#ifndef FRUITLIST_HPP
#define FRUITLIST_HPP

#include "TextureImage.hpp"

#include <cstdint>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

enum class FruitListError
{
    FILENOTFOUND,
    CANNOTLOADTEXTURE,
};

struct FruitSpec
{
    std::shared_ptr<TextureImage> texture;
    uint16_t diameter;
    uint16_t points;
};

class FruitList
{
public:
    ~FruitList() = default;

    static auto getInstance() -> FruitList&;

    auto getTexture(int level) -> std::shared_ptr<TextureImage>;
    auto getDiameter(int level) -> int;

    auto getPoints(int level) -> int;

    auto getMaxLevel() const -> int;
    auto open(const std::string& fn) -> std::optional<FruitListError>;

    auto getRandomLevel() const -> int;

private:
    FruitList()
    {
        open("gamedata/fruits.csv");
    }

    auto addTexture(const std::string&, int, int, int, int points) -> std::optional<FruitListError>;

    std::unordered_map<int, size_t> mTextureLookup;
    std::vector<FruitSpec> mFruits;
    int mMaxLevel{0};
};

#endif /* FRUITLIST */
