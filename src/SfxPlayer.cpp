#include "SfxPlayer.hpp"
#include "Screen.hpp"
#include "WAV.hpp"
#include "imemstream.hpp"

#include <cassert>
#include <cstring>

auto SfxPlayer::getInstance() -> SfxPlayer&
{
    static SfxPlayer instance;
    return instance;
}

auto SfxPlayer::addWAV(AssetFileHandle&& handle, const std::string& key) -> std::optional<SfxPlayerError>
{
    if (!Screen::getInstance().isAudioEnabled())
    {
        return std::nullopt;
    }

    const std::string content(handle.getData().begin(), handle.getData().end());
    imemstream stream(reinterpret_cast<const char*>(handle.getData().data()), handle.getData().size());

    WAV wav(stream);

    alGetError(); // reset errors

    ALuint buffer;
    alGenBuffers(1, &buffer);

    ALenum format;
    switch (wav.getFormat())
    {
    case PCMFormat::STEREO16:
        format = AL_FORMAT_STEREO16;
        break;
    case PCMFormat::MONO8:
        format = AL_FORMAT_MONO8;
        break;
    case PCMFormat::STEREO8:
        format = AL_FORMAT_STEREO8;
        break;
    }

    alBufferData(buffer, format, wav.getData(), wav.getSize(), wav.getRate());

    ALuint source;
    alGenSources(1, &source);
    alSourcei(source, AL_BUFFER, buffer);
    alSourcei(source, AL_SOURCE_RELATIVE, AL_FALSE);
    alSourcei(source, AL_REFERENCE_DISTANCE, 0.0F);

    mSfxLookup[key] = static_cast<int>(mSoundEffects.size());
    mSoundEffects.emplace_back(source, buffer);

    return std::nullopt;
}

void SfxPlayer::update()
{
    if (!Screen::getInstance().isAudioEnabled())
    {
        return;
    }

    int seen[mSoundEffects.size()];
    memset(&seen, 0, mSoundEffects.size() * sizeof(int));

    while (!mPlayQueue.empty())
    {
        const SampleRequest sample{mPlayQueue.front()};
        assert(sample.id < static_cast<int>(mSoundEffects.size()));

        if (seen[sample.id] < 1)
        {
            playSample(sample);
        }

        ++seen[sample.id];
        mPlayQueue.pop();
    }
}

void SfxPlayer::queueSample(const std::string& key)
{
    queueSample(key, 1.0F, 1.0F);
}

void SfxPlayer::queueSample(const std::string& key, float gain, float pitch)
{
    if (!Screen::getInstance().isAudioEnabled())
    {
        return;
    }

    SampleRequest req;
    req.id = mSfxLookup.at(key);
    req.pitch = pitch;
    req.gain = gain;
    queueSample(req);
}

void SfxPlayer::queueSample(SampleRequest req)
{
    if (!Screen::getInstance().isAudioEnabled())
    {
        return;
    }

    mPlayQueue.push(req);
}

auto SfxPlayer::playSample(SampleRequest req) -> std::optional<SfxPlayerError>
{
    if (!Screen::getInstance().isAudioEnabled())
    {
        return std::nullopt;
    }

    const ALuint source{mSoundEffects[req.id].first};

    alSourcef(source, AL_PITCH, req.pitch);
    alSourcef(source, AL_GAIN, mMasterVolume * req.gain);
    alSourcei(source, AL_LOOPING, 0);
    alSourcePlay(source);

    return std::nullopt;
}

void SfxPlayer::destroy()
{
    if (!Screen::getInstance().isAudioEnabled())
    {
        return;
    }

    for (auto sourceBufferPair : mSoundEffects)
    {
        alSourceStop(sourceBufferPair.first);
        alSourcei(sourceBufferPair.first, AL_BUFFER, 0); // unbind source
        alDeleteBuffers(1, &sourceBufferPair.second);
        alDeleteSources(1, &sourceBufferPair.first);
    }
    mSoundEffects.clear();
}

void SfxPlayer::setMasterVolume(float val)
{
    mMasterVolume = val;
}
