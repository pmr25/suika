#ifndef GAMELOGIC_HPP
#define GAMELOGIC_HPP

#include "Basket.hpp"
#include "Fruit.hpp"
#include "Screen.hpp"

#include <memory>
#include <unordered_map>
#include <vector>

struct FruitRequest
{
    glm::vec2 position;
    glm::vec2 velocity;
    int level;
};

class GameLogic
{
public:
    GameLogic(int w, int h)
    {
        init(w, h);
    }

    void init(int, int);
    void addFruit(const FruitRequest&);
    void addFruit(std::unique_ptr<Fruit>);
    void dropFruit(float x, float y);
    void update();
    void drawFruit(const Screen& screen) const;
    void previewNextFruit(float x, float y) const;

    [[nodiscard]] auto isGameOver() const -> bool;
    [[nodiscard]] auto getScore() const -> int;

private:
    void removeFruit(int index);
    void fuseFruit();

    bool mIsReady{true};
    bool mIsGameOver{false};
    int mPoints{0};
    std::unique_ptr<Fruit> mNextFruit;
    std::unique_ptr<Basket> mBasket;
    std::unordered_map<int, size_t> mFruitLookup;
    std::vector<std::unique_ptr<Fruit>> mFruit;
};

#endif /* GAMELOGIC_HPP */
