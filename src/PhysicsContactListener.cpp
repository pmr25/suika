#include "PhysicsContactListener.hpp"
#include "CollisionListener.hpp"

void PhysicsContactListener::BeginContact(b2Contact* contact)
{
    auto fixtureA{contact->GetFixtureA()};
    auto fixtureB{contact->GetFixtureB()};

    auto listenerA{static_cast<CollisionListener*>((void*)fixtureA->GetBody()->GetUserData().pointer)};
    auto listenerB{static_cast<CollisionListener*>((void*)fixtureB->GetBody()->GetUserData().pointer)};

    if (listenerA)
    {
        listenerA->notifyCollision(listenerB);
    }
    if (listenerB)
    {
        listenerB->notifyCollision(listenerA);
    }
}

void PhysicsContactListener::EndContact(b2Contact* contact)
{
}
