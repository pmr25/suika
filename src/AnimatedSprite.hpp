#ifndef ANIMATEDSPRITE_HPP
#define ANIMATEDSPRITE_HPP

#include "Animation.hpp"
#include "AnimationPlayer.hpp"
#include "Sprite.hpp"

#include <memory>
#include <utility>

enum class AnimatedSpriteError
{
    CannotSetAnimation,
};

class AnimatedSprite
{
public:
    AnimatedSprite(const Sprite& spr, std::shared_ptr<Animation> ani)
        : mSprite(spr)
    {
        setAnimationProvider(ani);
    }
    AnimatedSprite()
        : mSprite()
    {
    }
    ~AnimatedSprite() = default;

    auto setAnimationProvider(std::shared_ptr<Animation>&) -> void;
    auto setCurrentAnimation(const std::string&) -> std::optional<AnimatedSpriteError>;

    void update();

    auto getSprite() -> Sprite&;
    auto getAnimPlayer() -> AnimationPlayer&;

private:
    Sprite mSprite;
    AnimationPlayer mAnimPlayer;
};

#endif // ANIMATEDSPRITE_HPP
