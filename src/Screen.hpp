#ifndef SCREEN_HPP
#define SCREEN_HPP

#include "Sprite.hpp"

#include <SDL.h>
#include <alc.h>
#include <cstdint>

class Screen
{
public:
    ~Screen()
    {
        destroy();
    }

    static auto getInstance() -> Screen&;

    void create(uint32_t, uint32_t);
    void draw(const Sprite&) const;
    void draw(SDL_Texture*, SDL_Rect*, SDL_Rect*) const;
    [[nodiscard]] auto getRenderer() const -> SDL_Renderer*;

    void clear() const;
    void flip() const;

    [[nodiscard]] auto getWidth() const -> uint32_t;
    [[nodiscard]] auto getHeight() const -> uint32_t;
    [[nodiscard]] auto intersects(glm::vec4) const -> bool;
    [[nodiscard]] auto deviceToScreen(double) const -> double;
    [[nodiscard]] auto screenToDevice(double) const -> double;
    [[nodiscard]] auto isAudioEnabled() const -> bool;

private:
    Screen()
    {
        init();
    }

    auto destroy() -> void;
    auto init() -> void;

    SDL_Window* mWindow{nullptr};
    SDL_Renderer* mRenderer{nullptr};
    SDL_Surface* mWindowIcon{nullptr};
    uint32_t mWidth{0};
    uint32_t mHeight{0};
    double mScale{1.0};
    ALCdevice* mALDevice;
    ALCcontext* mALContext;
    bool mAudioEnabled{true};
};

#endif /* SCREEN_HPP */
