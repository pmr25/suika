#include "GameLogic.hpp"
#include "AppData.hpp"
#include "FruitList.hpp"

#include <random>
#include <set>

void GameLogic::addFruit(const FruitRequest& req)
{
    auto addition{std::make_unique<Fruit>(-1, req.level)};
    addition->getSprite().setCenter(req.position);
    addition->addPhysicsObject(req.velocity);
    addFruit(std::move(addition));
}

void GameLogic::addFruit(std::unique_ptr<Fruit> fruit)
{
    static int fruitIndex = 0;

    fruit->setIndex(fruitIndex);
    mFruit.push_back(std::move(fruit));
    mFruitLookup[fruitIndex] = mFruit.size() - 1;
    ++fruitIndex;
}

void GameLogic::dropFruit(float x, float y)
{
    if (!mIsReady || mIsGameOver)
    {
        return;
    }

    mNextFruit->getSprite().setOpacity(255);
    const auto pad{static_cast<float>(FruitList::getInstance().getDiameter(mNextFruit->getLevel())) / 2.0F};
    const glm::vec2 basketDim(mBasket->getBBox().z, mBasket->getBBox().w);
    const glm::vec2 basketPos(mBasket->getBBox().x, mBasket->getBBox().y);
    mNextFruit->getSprite().setCenter({fmaxf(basketPos.x + pad, fminf(basketPos.x + basketDim.x - pad, x)), y});

    std::mt19937 rng((std::random_device())());
    std::uniform_int_distribution<std::mt19937::result_type> dist(0, 2);
    mNextFruit->addPhysicsObject({int(dist(rng)) - 1, 50});
    addFruit(std::move(mNextFruit));

    mNextFruit = std::make_unique<Fruit>(-1, FruitList::getInstance().getRandomLevel());
    mNextFruit->getSprite().setOpacity(128);
    previewNextFruit(x, y);
}

void GameLogic::update()
{
    if (mIsGameOver)
    {
        return;
    }

    fuseFruit();

    mIsReady = true;
    for (auto& fruit : mFruit)
    {
        fruit->update();

        const auto fruitY{fruit->getSprite().getCenter().y - fruit->getDiameter() / 2.0F};

        mIsReady &= fruit->isActive() && fruitY > mBasket->getLimit();
        if (fruit->isStationary())
        {
            if (fruitY <= mBasket->getLimit())
            {
                auto& data = AppData::getInstance();
                data.setLastScore(mPoints);

                mIsGameOver = true;
                fruit->flag();
                if (mPoints > data.getHighScore())
                {
                    data.setHighScore(mPoints);
                }
            }
        }
    }
}

void GameLogic::drawFruit(const Screen& screen) const
{
    if (mIsReady && !mIsGameOver)
    {
        screen.draw(mNextFruit->getSprite());
    }

    for (const auto& fruit : mFruit)
    {
        screen.draw(fruit->getSprite());
    }

    screen.draw(mBasket->getSprite());
}

void GameLogic::previewNextFruit(float x, float y) const
{
    const glm::vec2 basketDim(mBasket->getBBox().z, mBasket->getBBox().w);
    const glm::vec2 basketPos(mBasket->getBBox().x, mBasket->getBBox().y);
    const auto pad{static_cast<float>(FruitList::getInstance().getDiameter(mNextFruit->getLevel())) / 2.0F};
    mNextFruit->getSprite().setCenter({fmaxf(basketPos.x + pad, fminf(basketPos.x + basketDim.x - pad, x)), y});
}

auto GameLogic::isGameOver() const -> bool
{
    return mIsGameOver;
}

auto GameLogic::getScore() const -> int
{
    return mPoints;
}

void GameLogic::init(int w, int h)
{
    mNextFruit = std::make_unique<Fruit>(0, FruitList::getInstance().getRandomLevel());
    mNextFruit->getSprite().setOpacity(128);
    mNextFruit->getSprite().setCenter({-100, -100});
    const auto border{6};
    const auto margin{48};
    mBasket = std::make_unique<Basket>(glm::vec4((Screen::getInstance().getWidth() - w) / 2,
                                                 Screen::getInstance().getHeight() - border - h - margin, w, h),
                                       border);
}

void GameLogic::removeFruit(int index)
{
    if (!mFruitLookup.contains(index))
    {
        return;
    }
    const auto realIndex{mFruitLookup[index]};

    mPoints += FruitList::getInstance().getPoints(mFruit[realIndex]->getLevel());

    mFruitLookup.erase(index);
    mFruit.erase(mFruit.begin() + realIndex);

    // reindex
    for (auto i = realIndex; i < mFruit.size(); ++i)
    {
        mFruitLookup[mFruit[i]->getIndex()] = i;
    }
}

void GameLogic::fuseFruit()
{
    std::set<int> toRemove;
    std::vector<FruitRequest> toAdd;

    for (auto& fruit : mFruit)
    {
        const auto fusedIndex{fruit->getFusedIndex()};
        if (toRemove.contains(fruit->getIndex()))
        {
            continue;
        }
        if (fusedIndex == -1)
        {
            continue;
        }

        toRemove.emplace(fruit->getIndex());
        toRemove.emplace(fruit->getFusedIndex());

        const auto nextLevel{fruit->getLevel() + 1};
        const auto& otherFruit{mFruit[mFruitLookup[fusedIndex]]};

        const auto pad{static_cast<float>(FruitList::getInstance().getDiameter(nextLevel)) / 2.0F};
        const auto center{0.5F * fruit->getSprite().getCenter() + 0.5F * otherFruit->getSprite().getCenter()};

        const glm::vec2 basketDim(mBasket->getBBox().z, mBasket->getBBox().w);
        const glm::vec2 basketPos(mBasket->getBBox().x, mBasket->getBBox().y);
        const glm::vec2 clampedCenter(fminf(basketPos.x + basketDim.x - pad, fmaxf(basketPos.x + pad, center.x)),
                                      fminf(basketPos.y + basketDim.y - pad, center.y));
        const glm::vec2 newVelocity{0.5F * fruit->getLastVelocity() + 0.5F * otherFruit->getLastVelocity()};
        toAdd.push_back({clampedCenter, newVelocity, nextLevel});
    }

    for (auto& index : toRemove)
    {
        removeFruit(index);
    }

    for (const auto& req : toAdd)
    {
        addFruit(req);
    }
}
