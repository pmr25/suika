#include "TickTimer.hpp"
#include "Clock.hpp"
#include <iostream>

auto TickTimer::setInterval(std::chrono::milliseconds dur) -> void
{
    mInterval = dur.count() / Clock::getInstance().getInterval();
    reset();
}

auto TickTimer::tick() -> bool
{
    if (mTicks == 0)
    {
        return true;
    }

    --mTicks;

    return false;
}

auto TickTimer::reset() -> void
{
    mTicks = mInterval;
}
