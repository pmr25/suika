#include "Assets.hpp"
#include "GameLoop.hpp"

#include <SDL.h>
#include <archive.h>
#include <archive_entry.h>

extern uint8_t assets[];
extern int assets_size;

void AssetFileHandle::initOps()
{
    mOps = SDL_RWFromConstMem(&mData[0], mData.size());
}

void AssetFileHandle::destroyOps()
{
    if (mOps != nullptr)
    {
        SDL_RWclose(mOps);
        mOps = nullptr;
    }
}

auto AssetFileHandle::getOps() -> SDL_RWops*
{
    return mOps;
}

auto AssetFileHandle::getData() -> const std::vector<uint8_t>&
{
    return mData;
}

auto Assets::getInstance() -> const Assets&
{
    static Assets instance;
    return instance;
}

auto Assets::getSize() const -> int
{
    return assets_size;
}

auto Assets::getFile(const std::string& fn) const -> AssetFileHandle
{
    auto data{openFile(fn)};
    return AssetFileHandle(std::move(data));
}

auto Assets::openFile(const std::string& fn) const -> std::vector<uint8_t>
{
    std::vector<uint8_t> data;

    auto archive{archive_read_new()};
    archive_read_support_filter_all(archive);
    archive_read_support_format_all(archive);

    const auto openErr{archive_read_open_memory(archive, assets, assets_size)};
    if (openErr != ARCHIVE_OK)
    {
        GameLoop::exit();
    }

    archive_entry* entry;
    while (archive_read_next_header(archive, &entry) == ARCHIVE_OK)
    {
        const std::string pathName(archive_entry_pathname(entry));
        if (pathName != fn)
        {
            continue;
        }

        const auto size = archive_entry_size(entry);
        auto buffer = static_cast<uint8_t*>(malloc(size));
        if (buffer == nullptr)
        {
            GameLoop::exit();
        }
        archive_read_data(archive, buffer, size);
        data.assign(buffer, buffer + size);

        free(buffer);
        break;
    }

    const auto finishErr{archive_read_free(archive)};
    if (finishErr != ARCHIVE_OK)
    {
        GameLoop::exit();
    }

    return data;
}
