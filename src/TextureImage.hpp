#ifndef TEXTUREIMAGE_HPP
#define TEXTUREIMAGE_HPP

#include "Assets.hpp"
#include "Texture.hpp"

#include <SDL.h>
#include <optional>
#include <string>

enum class TextureImageError
{
    FILENOTFOUND,
    CANNOTGENTEXTURE,
};

class TextureImage : public Texture
{
public:
    TextureImage() = default;
    explicit TextureImage(AssetFileHandle& handle)
    {
        open(handle);
    }
    explicit TextureImage(AssetFileHandle&& handle)
    {
        open(handle);
    }
    explicit TextureImage(const std::string& fileName)
    {
        open(fileName);
    }

    ~TextureImage() override
    {
        TextureImage::destroy();
    }

    auto open(const std::string& fileName) -> std::optional<TextureImageError>;
    auto open(AssetFileHandle&) -> std::optional<TextureImageError>;
    auto open(AssetFileHandle&&, int, int) -> std::optional<TextureImageError>;
    void destroy() override;
    [[nodiscard]] auto getDimensions() const -> const glm::vec2& override;
    void setDimensions(const glm::vec2&) override;
    [[nodiscard]] auto getSDLTexture() const -> SDL_Texture* override;

private:
    glm::vec2 mDimensions{0.0F};
    SDL_Surface* mSurface{nullptr};
    SDL_Texture* mTexture{nullptr};
};

#endif /* TEXTUREIMAGE_HPP */
