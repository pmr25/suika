#include "BoardState.hpp"
#include "AppData.hpp"
#include "Clock.hpp"
#include "Font.hpp"
#include "PhysicsWorld.hpp"
#include "Screen.hpp"
#include "SfxPlayer.hpp"

#include <SDL.h>
#include <iostream>
#include <sstream>

void BoardState::init()
{
    auto& data{AppData::getInstance()};
    data.reset();
    mGameLogic = std::make_unique<GameLogic>(400, 400);
    mFont = std::make_shared<Font>("fonts/ShantellSans-VariableFont_BNCE,INFM,SPAC,wght.ttf");
    mTextLine = std::make_unique<TextLine>(mFont);
    mTextLine->setPtSize(48);
    mTextLine->getSprite().setPosition({16, 0});

    SfxPlayer::getInstance().addWAV(Assets::getInstance().getFile("sound/ping.wav"), "ping");
    SfxPlayer::getInstance().addWAV(Assets::getInstance().getFile("sound/thud.wav"), "thud");

    GameStateBase::setReady(true);
}

void BoardState::handleEvent(SDL_Event* event)
{
    const auto& screen{Screen::getInstance()};

    if (event->type == SDL_MOUSEMOTION)
    {
        mGameLogic->previewNextFruit(static_cast<float>(screen.deviceToScreen(event->motion.x)), 50);
    }

    if (event->type == SDL_MOUSEBUTTONUP)
    {
        if (event->button.button != SDL_BUTTON_LEFT)
        {
            return;
        }
        mGameLogic->dropFruit(screen.deviceToScreen(event->button.x), 50);
    }
}

void BoardState::update()
{
    const auto [currentPhase, nextState] = GameStateBase::getCurrentPhase();
    if (currentPhase == GameStatePhase::ENTER)
    {
        init();
        setPhase(GameStatePhase::MAIN, false);
    }
    updateScore();
    mBackground.update();
    mGameLogic->update();
    mTextLine->update();
    if (mGameLogic->isGameOver())
    {
        GameStateBase::setNextState("end");
        setPhase(GameStatePhase::IDLE, false);
    }

    PhysicsWorld::getInstance().step(static_cast<float>(Clock::getInstance().getInterval()) / 1000.0F, 8, 3);
}

void BoardState::draw()
{
    const auto& screen{Screen::getInstance()};
    const auto [currentPhase, nextState] = GameStateBase::getCurrentPhase();

    for (const auto& sprite : mBackground.getSprites())
    {
        screen.draw(sprite);
    }

    if (currentPhase == GameStatePhase::MAIN)
    {
        mGameLogic->drawFruit(screen);

        screen.draw(mTextLine->getSprite());
    }
}

void BoardState::destroy()
{
}

void BoardState::updateScore()
{
    std::stringstream scoreStrBuilder;
    scoreStrBuilder << "Score: " << mGameLogic->getScore();
    const auto scoreStr{scoreStrBuilder.str()};
    mTextLine->setText(scoreStr);
}
