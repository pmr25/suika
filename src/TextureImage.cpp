#include "TextureImage.hpp"
#include "GfxUtils.hpp"
#include "Screen.hpp"

#include <SDL_image.h>
#include <iostream>

auto TextureImage::open(const std::string& fileName) -> std::optional<TextureImageError>
{
    mSurface = IMG_Load(fileName.c_str());
    if (mSurface == nullptr)
    {
        return TextureImageError::FILENOTFOUND;
    }
    setDimensions({mSurface->w, mSurface->h});

    mTexture = SDL_CreateTextureFromSurface(Screen::getInstance().getRenderer(), mSurface);
    if (mTexture == nullptr)
    {
        return TextureImageError::CANNOTGENTEXTURE;
    }

    SDL_FreeSurface(mSurface);
    mSurface = nullptr;

    return std::nullopt;
}

auto TextureImage::open(AssetFileHandle& handle) -> std::optional<TextureImageError>
{
    mSurface = IMG_Load_RW(handle.getOps(), 0);
    if (mSurface == nullptr)
    {
        return TextureImageError::FILENOTFOUND;
    }
    setDimensions({mSurface->w, mSurface->h});

    mTexture = SDL_CreateTextureFromSurface(Screen::getInstance().getRenderer(), mSurface);
    if (mTexture == nullptr)
    {
        return TextureImageError::CANNOTGENTEXTURE;
    }

    SDL_FreeSurface(mSurface);
    mSurface = nullptr;

    return std::nullopt;
}

auto TextureImage::open(AssetFileHandle&& handle, int screenX, int screenY) -> std::optional<TextureImageError>
{
    mSurface = IMG_Load_RW(handle.getOps(), 0);
    if (mSurface == nullptr)
    {
        return TextureImageError::FILENOTFOUND;
    }

    if (screenX > 0 && screenY > 0)
    {
        SDL_LockSurface(mSurface);

        const auto size{mSurface->h * mSurface->pitch};
        const auto pixels{(uint8_t*)(mSurface->pixels)};
        const auto& screen{Screen::getInstance()};

        std::vector<uint8_t> raw(pixels, pixels + size);
        std::vector<uint8_t> out;
        out = utils::gfx::shrinkImage(raw, mSurface->w, mSurface->h, mSurface->pitch / mSurface->w,
                                      static_cast<int>(screen.screenToDevice(screenX)),
                                      static_cast<int>(screen.screenToDevice(screenY)));

        SDL_UnlockSurface(mSurface);
        SDL_FreeSurface(mSurface);

        mSurface = SDL_CreateRGBSurfaceWithFormat(0, screenX, screenY, 32, SDL_PIXELFORMAT_RGBA32);
        if (mSurface == nullptr)
        {
            return TextureImageError::CANNOTGENTEXTURE;
        }

        SDL_LockSurface(mSurface);
        memcpy(mSurface->pixels, out.data(), out.size());
        SDL_UnlockSurface(mSurface);
    }

    setDimensions({mSurface->w, mSurface->h});

    mTexture = SDL_CreateTextureFromSurface(Screen::getInstance().getRenderer(), mSurface);
    if (mTexture == nullptr)
    {
        return TextureImageError::CANNOTGENTEXTURE;
    }

    SDL_FreeSurface(mSurface);
    mSurface = nullptr;

    return std::nullopt;
}

void TextureImage::destroy()
{
    if (mTexture != nullptr)
    {
        SDL_DestroyTexture(mTexture);
        mTexture = nullptr;
    }

    if (mSurface != nullptr)
    {
        SDL_FreeSurface(mSurface);
        mSurface = nullptr;
    }
}

auto TextureImage::getDimensions() const -> const glm::vec2&
{
    return mDimensions;
}

void TextureImage::setDimensions(const glm::vec2& dim)
{
    mDimensions = dim;
}

auto TextureImage::getSDLTexture() const -> SDL_Texture*
{
    return mTexture;
}
