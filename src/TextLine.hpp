#ifndef TEXTLINE_HPP
#define TEXTLINE_HPP

#include "Font.hpp"
#include "Sprite.hpp"
#include "TextureWrapper.hpp"

#include <memory>
#include <string>
#include <utility>

class TextLine
{
public:
    explicit TextLine(std::shared_ptr<Font> font)
        : mFont(std::move(font))
    {
        mTexWrapper = std::make_shared<TextureWrapper>();
        mSprite.setTexture(mTexWrapper);
    }
    ~TextLine() = default;

    void setPtSize(int);
    void setText(const std::string&);
    void update();

    [[nodiscard]] auto getSprite() -> Sprite&;

private:
    Sprite mSprite;
    std::shared_ptr<Font> mFont;
    std::shared_ptr<TextureWrapper> mTexWrapper;
    std::string mText;
    SDL_Texture* mTexture{nullptr};
    bool mIsDirty{false};
    int mPtSize{1000};
};

#endif // TEXTLINE_HPP
