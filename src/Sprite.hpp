#ifndef SPRITE_HPP
#define SPRITE_HPP

#include "Texture.hpp"

#include <cstdint>
#include <glm/glm.hpp>
#include <memory>

class Sprite
{
public:
    Sprite() = default;
    ~Sprite() = default;

    [[nodiscard]] auto getPosition() const -> const glm::vec2&;
    [[nodiscard]] auto getCenter() const -> glm::vec2;
    void setPosition(glm::vec2);
    void setCenter(glm::vec2);

    [[nodiscard]] auto getVelocity() const -> const glm::vec2&;
    void setVelocity(glm::vec2);

    [[nodiscard]] auto getTheta() const -> float;
    void setTheta(float t);

    [[nodiscard]] auto getOpacity() const -> uint8_t;
    void setOpacity(uint8_t);

    [[nodiscard]] auto getBBox() const -> glm::vec4;

    [[nodiscard]] auto getDimensions() const -> const glm::vec2&;
    void setDimensions(glm::vec2);

    void setTexture(const std::shared_ptr<Texture>&);
    [[nodiscard]] auto getTexture() const -> const std::shared_ptr<Texture>&;

    void setFrame(const glm::vec4&);
    [[nodiscard]] auto getFrame() const -> glm::vec4;

    void setColorMod(glm::vec3);
    [[nodiscard]] auto getColorMod() const -> const glm::vec3&;

    void update();

    void setVisible(bool b);
    [[nodiscard]] auto isVisible() const -> bool;

private:
    float mTheta{0.0F};
    uint8_t mOpacity{255};
    glm::vec2 mPosition{0.0F};
    glm::vec2 mVelocity{0.0F};
    glm::vec2 mDimensions{0.0F};
    glm::vec3 mColorMod{1.0F};
    glm::vec4 mFrame{0.0F};
    std::shared_ptr<Texture> mTexture;
    bool mIsVisible{true};
};

#endif /* SPRITE_HPP */
