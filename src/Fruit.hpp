#ifndef FRUIT_HPP
#define FRUIT_HPP

#include "CollisionListener.hpp"
#include "PhysicsWorld.hpp"
#include "Sprite.hpp"

class Fruit final : public CollisionListener
{
public:
    explicit Fruit(int id = -1, int level = 1)
        : index(id)
        , mLevel(level)
    {
        init();
    }

    ~Fruit()
    {
        PhysicsWorld::getInstance().deletePhysicsObject(mPhysObjId);
    }

    Fruit(Fruit&& other) noexcept = delete;
    Fruit& operator=(Fruit&& other) noexcept = delete;
    Fruit(const Fruit& other) = delete;
    Fruit& operator=(Fruit& other) = delete;

    void init();
    void flag();
    void addPhysicsObject(const glm::vec2&);
    void setPhysicsObject(int);
    void update();
    auto getSprite() -> Sprite&;
    void notifyCollision(CollisionListener*) override;

    [[nodiscard]] auto getLevel() const -> int;
    void setIndex(int);
    [[nodiscard]] auto getIndex() const -> int;
    [[nodiscard]] auto getFusedIndex() const -> int;
    [[nodiscard]] auto isActive() const -> bool;
    [[nodiscard]] auto getDiameter() const -> float;
    [[nodiscard]] auto isStationary() const -> bool;
    [[nodiscard]] auto getVelocity() const -> glm::vec2;
    [[nodiscard]] auto getLastVelocity() const -> const glm::vec2&;

private:
    void tempMuteSfx();
    void playSound(const std::string&) const;

    Sprite mSprite;
    glm::vec2 mLastVelocity{0.0F};
    int mPhysObjId{-1};
    int index{-1};
    int mLevel{1};
    int fused{-1};
    bool mIsActive{false};
    bool mIsMuted{false};
};

#endif /* FRUIT_HPP */
