#include "Background.hpp"
#include "Assets.hpp"
#include "Screen.hpp"
#include "TextureImage.hpp"

#include <cmath>
#include <random>

void Background::init()
{
    auto bgTex{std::make_shared<TextureImage>(Assets::getInstance().getFile("textures/background.png"))};
    mBubbleTex = std::static_pointer_cast<Texture>(
        std::make_shared<TextureImage>(Assets::getInstance().getFile("textures/bubble.png")));

    Sprite bg;
    bg.setTexture(bgTex);
    bg.setDimensions({Screen::getInstance().getWidth(), Screen::getInstance().getHeight()});
    mSprites.push_back(bg);

    for (int i = 0; i < 10; ++i)
    {
        addBubble();
    }
}

void Background::update()
{
    for (const auto& bubbleIndex : mBubbles)
    {
        auto& bubble{mSprites[bubbleIndex]};
        bubble.update();
        if (!Screen::getInstance().intersects(bubble.getBBox()))
        {
            resetBubble(bubbleIndex);
        }
    }
}

auto Background::getSprites() const -> const std::vector<Sprite>&
{
    return mSprites;
}

void Background::addBubble()
{
    Sprite bubble;
    bubble.setOpacity(64);
    bubble.setTexture(mBubbleTex);
    mSprites.push_back(bubble);
    mBubbles.push_back(mSprites.size() - 1);
    resetBubble(mBubbles.back());
}

void Background::resetBubble(size_t idx)
{
    std::mt19937 rng((std::random_device())());
    std::uniform_int_distribution<std::mt19937::result_type> angleDist(0, 359);
    std::uniform_int_distribution<std::mt19937::result_type> qtrAngleDist(0, 90);
    std::uniform_int_distribution<std::mt19937::result_type> radiusDist(0, Screen::getInstance().getWidth());
    std::uniform_int_distribution<std::mt19937::result_type> velocityDist(50, 100);

    const auto startAngle{angleDist(rng)};
    const auto moveAngle{angleDist(rng)};
    const auto radius{float(radiusDist(rng))};
    const double velocity{double(velocityDist(rng)) / 200.F};
    const double startX{radius * cosf(double(startAngle) * M_PI / 180.0) + Screen::getInstance().getWidth() / 2};
    const double startY{radius * sinf(double(startAngle) * M_PI / 180.0) + Screen::getInstance().getHeight() / 2};
    const double velX{velocity * cosf(double(moveAngle) * M_PI / 180.0)};
    const double velY{velocity * sinf(double(moveAngle) * M_PI / 180.0)};

    const double colorTheta{double(qtrAngleDist(rng)) * M_PI / 180.0};
    const double colorPhi{double(qtrAngleDist(rng)) * M_PI / 180.0};
    const double r{sinf(colorTheta) * cosf(colorPhi) * 0.5 + 0.5};
    const double g{sinf(colorTheta) * sinf(colorPhi) * 0.5 + 0.5};
    const double b{cosf(colorTheta) * 0.5 + 0.5};

    auto& bubble{mSprites[idx]};
    bubble.setPosition({startX, startY});
    bubble.setDimensions(glm::vec2(128));
    bubble.setVelocity({velX, velY});
    bubble.setColorMod({r, g, b});
}
