#ifndef COLLISIONLISTENER_HPP
#define COLLISIONLISTENER_HPP

class CollisionListener
{
public:
    virtual void notifyCollision(CollisionListener*) = 0;
};

#endif /* COLLISIONLISTENER_HPP */