#include "TitleState.hpp"
#include "Assets.hpp"
#include "GameLoop.hpp"
#include "Screen.hpp"
#include "TextureImage.hpp"

void TitleState::init()
{
    const auto titleTex{std::make_shared<TextureImage>(Assets::getInstance().getFile("textures/title.png"))};
    mTitle.setTexture(titleTex);
    mTitle.setDimensions({500, 500});
    mTitle.setCenter({Screen::getInstance().getWidth() / 2, 300});

    auto animation{std::make_shared<Animation>()};
    auto animHandle{Assets::getInstance().getFile("animations/two_frame_button.xml")};
    animation->open(animHandle);

    auto startButtonTex{std::make_shared<TextureImage>()};
    startButtonTex->open(Assets::getInstance().getFile("textures/button_start.png"), 128, 128);
    mStartButton.getSprite().getSprite().setTexture(startButtonTex);
    mStartButton.getSprite().getSprite().setDimensions({128, 64});
    mStartButton.getSprite().setAnimationProvider(animation);
    mStartButton.getSprite().getSprite().setCenter(
        {Screen::getInstance().getWidth() * 11 / 16, Screen::getInstance().getHeight() * 3 / 4});

    auto quitButtonTex{std::make_shared<TextureImage>()};
    quitButtonTex->open(Assets::getInstance().getFile("textures/button_quit.png"), 128, 128);
    mQuitButton.getSprite().getSprite().setTexture(quitButtonTex);
    mQuitButton.getSprite().getSprite().setDimensions({128, 64});
    mQuitButton.getSprite().setAnimationProvider(animation);
    mQuitButton.getSprite().getSprite().setCenter(
        {Screen::getInstance().getWidth() * 5 / 16, Screen::getInstance().getHeight() * 3 / 4});

    GameStateBase::setReady(true);
}

void TitleState::handleEvent(SDL_Event* event)
{
    mStartButton.handleEvent(event);
    mQuitButton.handleEvent(event);
}

void TitleState::update()
{
    mBackground.update();
    mStartButton.update();
    mQuitButton.update();

    if (mStartButton.isClicked())
    {
        GameStateBase::setNextState("board");
        GameStateBase::setPhase(GameStatePhase::IDLE, false);
    }

    if (mQuitButton.isClicked())
    {
        GameStateBase::setNextState("quit");
        GameStateBase::setPhase(GameStatePhase::IDLE, false);
    }
}

void TitleState::draw()
{
    const auto& screen{Screen::getInstance()};

    for (auto& sprite : mBackground.getSprites())
    {
        screen.draw(sprite);
    }
    screen.draw(mTitle);

    screen.draw(mStartButton.getSprite().getSprite());
    screen.draw(mQuitButton.getSprite().getSprite());
}
