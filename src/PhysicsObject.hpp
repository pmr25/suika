#ifndef PHYSICSOBJECT_HPP
#define PHYSICSOBJECT_HPP

#include "CollisionListener.hpp"

#include <box2d/box2d.h>
#include <glm/glm.hpp>
#include <vector>

class PhysicsObject
{
public:
    PhysicsObject(b2World* world, bool isDynamic, CollisionListener* listener, float scale = 0.1F)
        : mScale(scale)
    {
        createBody(world, isDynamic, listener);
    }
    void createBody(b2World* world, bool, CollisionListener*);
    void destroyBody(b2World* world);
    void createRect(float x, float y, float w, float h);
    void createRects(const std::vector<glm::vec4>& rects);
    void createPolygon(const std::vector<glm::vec2>& points);
    void createCircle(float x, float y, float radius);
    [[nodiscard]] auto getB2DBody() const -> b2Body*;
    [[nodiscard]] auto getPosition() const -> glm::vec2;
    [[nodiscard]] auto getVelocity() const -> glm::vec2;
    void setVelocity(const glm::vec2&);
    [[nodiscard]] auto getTheta() const -> float;
    void setListener(CollisionListener* listener);

private:
    b2Body* mBody{nullptr};
    float mScale{0.1F};
};

#endif /* PHYSICSOBJECT_HPP */