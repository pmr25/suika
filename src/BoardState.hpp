#ifndef BOARDSTATE_HPP
#define BOARDSTATE_HPP

#include "Background.hpp"
#include "GameLogic.hpp"
#include "GameStateBase.hpp"
#include "TextLine.hpp"

#include <memory>

class Font;
class BoardState final : public GameStateBase
{
public:
    BoardState()
    {
        BoardState::init();
    }
    ~BoardState() override
    {
        destroy();
    }
    BoardState(const BoardState& other) = delete;
    BoardState(BoardState&& other) noexcept
        : GameStateBase(std::move(other))
        , mGameLogic(std::move(other.mGameLogic))
        , mBackground(std::move(other.mBackground))
        , mFont(other.mFont)
    {
        other.mFont = nullptr;
    }
    BoardState& operator=(const BoardState& other) = delete;
    BoardState& operator=(BoardState&& other) noexcept
    {
        if (this == &other)
            return *this;
        GameState::operator=(std::move(other));
        mGameLogic = std::move(other.mGameLogic);
        mBackground = std::move(other.mBackground);
        mFont = other.mFont;
        other.mFont = nullptr;
        return *this;
    }

    void init() override;
    void handleEvent(SDL_Event*) override;
    void update() override;
    void draw() override;
    void destroy();

private:
    void updateScore();

    std::unique_ptr<GameLogic> mGameLogic;
    Background mBackground;
    std::shared_ptr<Font> mFont;
    std::unique_ptr<TextLine> mTextLine;
};

#endif // BOARDSTATE_HPP
