#include "Basket.hpp"
#include "PhysicsWorld.hpp"
#include "TextureImage.hpp"

#include <glm/gtx/string_cast.hpp>
#include <vector>

void Basket::init(const glm::vec4& bbox)
{
    mSprite.setTexture(std::make_shared<TextureImage>(Assets::getInstance().getFile("textures/basket.png")));
    mSprite.setPosition({bbox.x - mBorder, bbox.y - mBorder});
    mSprite.setDimensions({bbox.z + mBorder * 2, bbox.w + mBorder * 2});
    mBox = bbox;
}

void Basket::addPhysicsObject()
{
    mPhysObj = PhysicsWorld::getInstance().createPhysicsObject(1.0, false);
    auto obj = PhysicsWorld::getInstance().getPhysicsObject(mPhysObj);
    if (obj == nullptr)
    {
        return;
    }

    obj->getB2DBody()->SetTransform(
        b2Vec2(mBox.x * PhysicsWorld::getInstance().getScale(), mBox.y * PhysicsWorld::getInstance().getScale()), 0);

    const auto width{mBox.z};
    const auto halfWidth{width / 2.0F};
    const auto height{mBox.w};
    const auto halfHeight{height / 2.0F};
    const auto pad{10};

    std::vector<glm::vec4> rects;
    rects.reserve(3);
    rects.emplace_back(halfWidth, height + pad, halfWidth, pad);
    rects.emplace_back(-pad - 1, halfHeight, pad, halfHeight);
    rects.emplace_back(width + pad + 1, halfHeight, pad, halfHeight);

    obj->createRects(rects);
}

auto Basket::getSprite() -> Sprite&
{
    return mSprite;
}

auto Basket::getBBox() const -> const glm::vec4&
{
    return mBox;
}

auto Basket::getLimit() const -> float
{
    return mBox.y;
}
