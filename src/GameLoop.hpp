#ifndef GAMELOOP_HPP
#define GAMELOOP_HPP

#include "GameState.hpp"

#include <functional>
#include <memory>
#include <unordered_map>

class GameLoop
{
public:
    GameLoop()
    {
    }
    ~GameLoop() = default;

    void addState(const std::string&, std::function<std::unique_ptr<GameState>()>);
    void mainLoop();

    static void exit();
    static auto shouldExit() -> bool;

private:
    void setCurrentState(const std::string& dest);
    void setNextState(const std::string& dest);

    void update();
    void draw() const;

    std::unique_ptr<GameState> mCurrentState;
    std::unique_ptr<GameState> mNextState;
    std::unordered_map<std::string, std::function<std::unique_ptr<GameState>()>> mStateCreators;
};

#endif /* GAMELOOP_HPP */