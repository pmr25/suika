#ifndef ENDSTATE_HPP
#define ENDSTATE_HPP

#include "Background.hpp"
#include "Button.hpp"
#include "Font.hpp"
#include "GameStateBase.hpp"
#include "TextLine.hpp"

#include <memory>

class EndState final : public GameStateBase
{
public:
    EndState()
    {
        init();
    }
    ~EndState() override = default;

    void init() override;
    void handleEvent(SDL_Event* event) override;
    void update() override;
    void draw() override;

private:
    std::shared_ptr<Font> mFont;
    std::unique_ptr<TextLine> mCurrentScoreText;
    std::unique_ptr<TextLine> mHighScoreText;
    Background mBackground;
    Button mContinueButton;
};

#endif // ENDSTATE_HPP
