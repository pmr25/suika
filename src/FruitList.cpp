#include "FruitList.hpp"
#include "Assets.hpp"

#include <cmath>
#include <iostream>
#include <random>
#include <sstream>
#include <string>
#include <vector>

auto FruitList::getInstance() -> FruitList&
{
    static FruitList instance;
    return instance;
}

auto FruitList::getTexture(int level) -> std::shared_ptr<TextureImage>
{
    assert(mTextureLookup.find(level) != mTextureLookup.end());
    return mFruits[mTextureLookup[level]].texture;
}

auto FruitList::getDiameter(int level) -> int
{
    assert(mTextureLookup.find(level) != mTextureLookup.end());
    return mFruits[mTextureLookup[level]].diameter;
}

auto FruitList::getPoints(int level) -> int
{
    return mFruits[mTextureLookup[level]].points;
}

auto FruitList::getMaxLevel() const -> int
{
    return mMaxLevel;
}

auto FruitList::open(const std::string& fn) -> std::optional<FruitListError>
{
    const auto data{Assets::getInstance().getFile(fn).getData()};
    std::istringstream in(std::string(data.begin(), data.end()));

    std::vector<std::string> columns;
    for (std::string line; std::getline(in, line);)
    {
        for (std::size_t head{0}; head < line.size();)
        {
            const auto sub{line.substr(head)};
            auto tail{sub.find(',')};
            if (tail == std::string::npos)
            {
                tail = line.size();
            }
            const auto token{sub.substr(0, tail)};
            columns.push_back(token);
            head += tail + 1;
        }

        if (columns.size() == 4)
        {
            const auto level{std::atoi(columns[0].c_str())};
            const auto width{std::atoi(columns[1].c_str())};
            const auto points{std::atoi(columns[2].c_str())};
            const auto addErr{addTexture(columns[3], level, width, width * 3 / 4, points)};
            if (addErr)
            {
                return addErr;
            }
        }
        else
        {
            std::cout << "bad line " << line << std::endl;
        }

        columns.clear();
    }

    return std::nullopt;
}

auto FruitList::getRandomLevel() const -> int
{
    std::mt19937 rng((std::random_device())());
    std::uniform_int_distribution<std::mt19937::result_type> dist(1, 4);
    return dist(rng);
}

auto FruitList::addTexture(const std::string& fn, int level, int width, int diameter, int points)
    -> std::optional<FruitListError>
{
    FruitSpec spec;
    spec.diameter = diameter;
    spec.texture = std::make_shared<TextureImage>();
    spec.points = points;
    if (const auto openErr{spec.texture->open(Assets::getInstance().getFile(fn), width, width)})
    {
        return FruitListError::CANNOTLOADTEXTURE;
    }
    spec.texture->setDimensions({width, width});
    mFruits.push_back(spec);

    mTextureLookup[level] = mFruits.size() - 1;

    mMaxLevel = static_cast<int>(fmax(level, mMaxLevel));

    return std::nullopt;
}
