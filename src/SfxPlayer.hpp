#ifndef SUIKA_SFXPLAYER_HPP
#define SUIKA_SFXPLAYER_HPP

#include "Assets.hpp"

#include <al.h>
#include <optional>
#include <queue>
#include <string>
#include <unordered_map>
#include <vector>

enum class SfxPlayerError
{
    CannotOpenWAV,
    CannotPlaySample
};

struct SampleRequest
{
    int id;
    float gain;
    float pitch;
};

class SfxPlayer
{
public:
    static auto getInstance() -> SfxPlayer&;

    ~SfxPlayer()
    {
        destroy();
    }
    SfxPlayer(const SfxPlayer& other) = delete;
    auto operator=(const SfxPlayer& other) -> SfxPlayer& = delete;
    SfxPlayer(SfxPlayer&& other) = delete;
    auto operator=(const SfxPlayer&& other) -> SfxPlayer& = delete;

    auto addWAV(AssetFileHandle&&, const std::string& key) -> std::optional<SfxPlayerError>;
    void queueSample(const std::string& key);
    void queueSample(const std::string& key, float, float);
    void queueSample(SampleRequest);
    void update();
    auto playSample(SampleRequest) -> std::optional<SfxPlayerError>;

    void setMasterVolume(float);

    void destroy();

private:
    SfxPlayer() = default;

    std::unordered_map<std::string, int> mSfxLookup;
    std::vector<std::pair<ALuint, ALuint>> mSoundEffects;
    std::queue<SampleRequest> mPlayQueue;
    float mMasterVolume{1.0F};
};

#endif // SUIKA_SFXPLAYER_HPP
