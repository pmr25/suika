#include "AppData.hpp"

#include <cereal/archives/xml.hpp>
#include <cstdlib>
#include <filesystem>
#include <fstream>

auto AppData::getInstance() -> AppData&
{
    const auto appdataDir = std::filesystem::path(getAppDataDir());
    const auto appdataFile = appdataDir / std::filesystem::path("appdata");
    if (!is_directory(appdataDir) || !exists(appdataDir))
    {
        create_directory(appdataDir);
    }

    static AppData data(appdataFile.c_str());

    return data;
}

auto AppData::getAppDataDir() -> std::string
{
#ifdef __linux__
    const std::filesystem::path homeDir(getenv("HOME"));
    const auto appdataDir = homeDir / std::filesystem::path(".local/share/suikagame");
    return appdataDir;
#endif /* __linux__ */

    return ".";
}

auto AppData::getHighScore() const -> int
{
    return mContent.highScore;
}

void AppData::setHighScore(int val)
{
    mContent.highScore = val;
    mNewHighScoreFlag = true;
}

void AppData::load()
{
    std::ifstream in(mFilename);
    if (!in)
    {
        return;
    }

    try
    {
        cereal::XMLInputArchive archive(in);
        archive(mContent);
    }
    catch (cereal::Exception& ex)
    {
        std::filesystem::remove(mFilename);
    }
}

void AppData::save()
{
    std::ofstream out(mFilename);
    if (!out)
    {
        return;
    }

    cereal::XMLOutputArchive archive(out);
    archive(mContent);
}
auto AppData::getLastScore() const -> int
{
    return mContent.lastScore;
}
void AppData::setLastScore(int val)
{
    mContent.lastScore = val;
}
auto AppData::hasNewHighScore() const -> bool
{
    return mNewHighScoreFlag;
}
void AppData::reset()
{
    mNewHighScoreFlag = false;
}
