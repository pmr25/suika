#ifndef APPDATA_HPP
#define APPDATA_HPP

#include <string>

class AppDataContent
{
public:
    template <class Archive>
    void serialize(Archive& ar)
    {
        ar(highScore, lastScore);
    }

    int highScore{0};
    int lastScore{0};
};

class AppData
{
public:
    static auto getInstance() -> AppData&;
    static auto getAppDataDir() -> std::string;

    ~AppData()
    {
        save();
    }

    [[nodiscard]] auto getHighScore() const -> int;
    void setHighScore(int);

    [[nodiscard]] auto getLastScore() const -> int;
    void setLastScore(int);

    [[nodiscard]] auto hasNewHighScore() const -> bool;

    void reset();

private:
    explicit AppData(const std::string& fn)
        : mFilename(fn)
    {
        load();
    }

    void load();
    void save();

    std::string mFilename;
    AppDataContent mContent;
    bool mNewHighScoreFlag{false};
};

#endif // APPDATA_HPP
