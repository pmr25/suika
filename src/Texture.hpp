#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <SDL.h>
#include <glm/glm.hpp>

class Texture
{
public:
    virtual ~Texture() = default;
    virtual void destroy() = 0;
    [[nodiscard]] virtual auto getDimensions() const -> const glm::vec2& = 0;
    virtual void setDimensions(const glm::vec2&) = 0;
    [[nodiscard]] virtual auto getSDLTexture() const -> SDL_Texture* = 0;
};

#endif /* TEXTURE_HPP */