#include "GameStateBase.hpp"
auto GameStateBase::isReady() const -> bool
{
    return mIsReady;
}
void GameStateBase::setReady(bool val)
{
    mIsReady = val;
}
void GameStateBase::setPhase(GameStatePhase phase, bool forceUpdate)
{
    mCurrentPhase = phase;
    if (forceUpdate)
    {
        update();
    }
}
auto GameStateBase::getCurrentPhase() const -> std::pair<GameStatePhase, std::string>
{
    return {mCurrentPhase, mNextState};
}
void GameStateBase::setNextState(const std::string& state)
{
    mNextState = state;
}
