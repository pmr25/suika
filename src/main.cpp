#include "BoardState.hpp"
#include "EndState.hpp"
#include "GameLoop.hpp"
#include "Logger.hpp"
#include "Screen.hpp"
#include "TitleState.hpp"

auto main() -> int
{
    if (!logger::init())
    {
        return 1;
    }

    Screen::getInstance().create(800, 600);

    GameLoop game;
    game.addState("title", [] { return std::make_unique<TitleState>(); });
    game.addState("board", [] { return std::make_unique<BoardState>(); });
    game.addState("end", [] { return std::make_unique<EndState>(); });
    game.addState("quit", [] { return nullptr; });
    game.mainLoop();
}