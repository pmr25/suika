#include "GameLoop.hpp"
#include "Clock.hpp"
#include "Screen.hpp"
#include "SfxPlayer.hpp"

#include <SDL.h>
#include <utility>

static bool exitFlag = false;

void GameLoop::update()
{
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        if (event.type == SDL_QUIT)
        {
            exit();
            return;
        }

        if (mCurrentState == nullptr)
        {
            return;
        }
        if (!mCurrentState->isReady())
        {
            return;
        }
        mCurrentState->handleEvent(&event);

        if (mNextState != nullptr && mNextState->isReady())
        {
            mNextState->handleEvent(&event);
        }
    }

    if (mCurrentState != nullptr && mCurrentState->isReady())
    {
        mCurrentState->update();
        const auto [phase, dest] = mCurrentState->getCurrentPhase();
        if (phase == GameStatePhase::IDLE)
        {
            setCurrentState(dest);
            mCurrentState->setPhase(GameStatePhase::ENTER, true);
        }
        else if (phase == GameStatePhase::LEAVE)
        {
            setNextState(dest);
        }
    }

    if (mNextState != nullptr && mNextState->isReady())
    {
        mNextState->update();
    }

    SfxPlayer::getInstance().update();
}

void GameLoop::draw() const
{
    if (mCurrentState != nullptr && mCurrentState->isReady())
    {
        mCurrentState->draw();
    }

    if (mNextState != nullptr && mNextState->isReady())
    {
        mNextState->draw();
    }
}

void GameLoop::mainLoop()
{
    const auto& screen{Screen::getInstance()};
    auto& clock{Clock::getInstance()};

    while (!shouldExit())
    {
        screen.clear();
        draw();
        screen.flip();
        clock.tick();
        while (clock.isLagging())
        {
            update();
            clock.lagTick();
        }
    }
}

void GameLoop::setCurrentState(const std::string& dest)
{
    const std::unique_ptr<GameState>& next{mStateCreators[dest]()};
    if (!next)
    {
        exit();
        return;
    }

    mCurrentState = mStateCreators[dest]();
    mCurrentState->update();
}

void GameLoop::setNextState(const std::string& dest)
{
    mNextState = mStateCreators[dest]();
}

void GameLoop::addState(const std::string& key, std::function<std::unique_ptr<GameState>()> func)
{
    mStateCreators[key] = std::move(func);
    if (!mCurrentState)
    {
        setCurrentState(key);
    }
}

void GameLoop::exit()
{
    exitFlag = true;
}

auto GameLoop::shouldExit() -> bool
{
    return exitFlag;
}
