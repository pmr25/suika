#ifndef SUIKA_TICKTIMER_HPP
#define SUIKA_TICKTIMER_HPP

#include <chrono>
#include <cstdint>

class TickTimer
{
public:
    explicit TickTimer(std::chrono::milliseconds dur = std::chrono::milliseconds(0))
    {
        setInterval(dur);
    }
    ~TickTimer() = default;

    auto setInterval(std::chrono::milliseconds dur) -> void;
    [[nodiscard]] auto tick() -> bool;
    auto reset() -> void;

private:
    uint64_t mInterval{0};
    uint64_t mTicks{0};
};

#endif // SUIKA_TICKTIMER_HPP
