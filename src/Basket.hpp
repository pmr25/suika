#ifndef BASKET_HPP
#define BASKET_HPP

#include "Sprite.hpp"

class Basket
{
public:
    Basket(const glm::vec4& bbox, int border)
        : mBorder(border)
    {
        init(bbox);
        addPhysicsObject();
    }

    void init(const glm::vec4& bbox);
    void addPhysicsObject();
    auto getSprite() -> Sprite&;
    [[nodiscard]] auto getBBox() const -> const glm::vec4&;
    [[nodiscard]] auto getLimit() const -> float;

private:
    Sprite mSprite;
    int mPhysObj{-1};
    int mBorder{10};
    glm::vec4 mBox{0};
};

#endif /* BASKET_HPP */