#include "TextureWrapper.hpp"

void TextureWrapper::destroy()
{
    if (mTexture != nullptr)
    {
        SDL_DestroyTexture(mTexture);
        mTexture = nullptr;
    }
}

auto TextureWrapper::getDimensions() const -> const glm::vec2&
{
    return mDimensions;
}

void TextureWrapper::setDimensions(const glm::vec2& vec)
{
    mDimensions = vec;
}

auto TextureWrapper::getSDLTexture() const -> SDL_Texture*
{
    return mTexture;
}

void TextureWrapper::setSDLTexture(SDL_Texture* t, int w, int h)
{
    setDimensions({w, h});
    mTexture = t;
}