#include "TextLine.hpp"
#include "Screen.hpp"

void TextLine::setPtSize(int val)
{
    if (mPtSize == val)
    {
        return;
    }

    mPtSize = val;
    mIsDirty = true;
}
void TextLine::setText(const std::string& text)
{
    if (text == mText)
    {
        return;
    }

    mText = text;
    mIsDirty = true;
}

void TextLine::update()
{
    if (!mIsDirty)
    {
        return;
    }

    mSprite.setVisible(!mText.empty());
    const auto& screen{Screen::getInstance()};
    constexpr SDL_Color fg{0, 0, 0};
    const auto surface = TTF_RenderText_Blended(mFont->getFont(), mText.c_str(), fg);
    if (surface == nullptr)
    {
        return;
    }

    if (mTexture != nullptr)
    {
        SDL_DestroyTexture(mTexture);
    }

    mTexture = SDL_CreateTextureFromSurface(screen.getRenderer(), surface);

    const auto scale = double(mPtSize) / double(mFont->getPtSize());
    mTexWrapper->setSDLTexture(mTexture, surface->w, surface->h);
    mSprite.setDimensions({surface->w * scale, surface->h * scale});

    SDL_FreeSurface(surface);

    mIsDirty = false;
}

auto TextLine::getSprite() -> Sprite&
{
    return mSprite;
}
