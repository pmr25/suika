#ifndef TITLESTATE_HPP
#define TITLESTATE_HPP

#include "AnimatedSprite.hpp"
#include "Background.hpp"
#include "Button.hpp"
#include "GameStateBase.hpp"
#include "Sprite.hpp"

class TitleState final : public GameStateBase
{
public:
    TitleState()
    {
        TitleState::init();
    }
    ~TitleState() override = default;
    void init() override;
    void handleEvent(SDL_Event*) override;
    void update() override;
    void draw() override;

private:
    Background mBackground;
    Sprite mTitle;
    AnimatedSprite test;
    Button mStartButton;
    Button mQuitButton;
};

#endif // TITLESTATE_HPP
