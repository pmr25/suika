#include "PhysicsObject.hpp"

void PhysicsObject::createBody(b2World* world, bool isDynamic, CollisionListener* listener)
{
    b2BodyDef bodyDef;
    bodyDef.position.Set(0.0F, 0.0F);
    bodyDef.type = isDynamic ? b2_dynamicBody : b2_staticBody;
    mBody = world->CreateBody(&bodyDef);
    mBody->GetUserData().pointer = reinterpret_cast<uintptr_t>(listener);
}

void PhysicsObject::destroyBody(b2World* world)
{
    world->DestroyBody(mBody);
}

void PhysicsObject::createRect(float x, float y, float w, float h)
{
    mBody->SetTransform(b2Vec2(x * mScale, y * mScale), mBody->GetAngle());
    b2PolygonShape shape;
    shape.SetAsBox(w / 2 * mScale, h / 2 * mScale);

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.density = 4.0F;
    fixtureDef.friction = 0.2F;

    mBody->CreateFixture(&fixtureDef);
}

void PhysicsObject::createRects(const std::vector<glm::vec4>& rects)
{
    b2PolygonShape shape;
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.density = 1.0F;
    fixtureDef.friction = 2.0F;

    for (const auto& r : rects)
    {
        const auto sr{mScale * r};
        shape.SetAsBox(sr.z, sr.w, b2Vec2(sr.x, sr.y), 0.0F);
        mBody->CreateFixture(&fixtureDef);
    }
}

void PhysicsObject::createPolygon(const std::vector<glm::vec2>& points)
{
    b2PolygonShape shape;
    std::vector<b2Vec2> b2Points;
    b2Points.reserve(points.size());
    for (const auto& p : points)
    {
        const auto sp{mScale * p};
        b2Points.emplace_back(sp.x, sp.y);
    }
    shape.Set(&b2Points[0], b2Points.size());

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.density = 1.0F;
    fixtureDef.friction = 0.2F;

    mBody->CreateFixture(&fixtureDef);
}

void PhysicsObject::createCircle(float x, float y, float radius)
{
    mBody->SetTransform(b2Vec2(x * mScale, y * mScale), mBody->GetAngle());
    b2CircleShape circle;
    circle.m_radius = radius * mScale * 0.75 * 0.5;

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &circle;
    fixtureDef.density = 1.0F;
    fixtureDef.friction = 0.01F;

    mBody->CreateFixture(&fixtureDef);
}

auto PhysicsObject::getB2DBody() const -> b2Body*
{
    return mBody;
}

auto PhysicsObject::getPosition() const -> glm::vec2
{
    const auto& pos{mBody->GetPosition()};
    return {pos.x / mScale, pos.y / mScale};
}

auto PhysicsObject::getTheta() const -> float
{
    return mBody->GetAngle();
}

void PhysicsObject::setListener(CollisionListener* listener)
{
    mBody->GetUserData().pointer = reinterpret_cast<uintptr_t>(listener);
}
auto PhysicsObject::getVelocity() const -> glm::vec2
{
    assert(mBody != nullptr);
    const auto& vel{mBody->GetLinearVelocity()};
    return {vel.x / mScale, vel.y / mScale};
}

void PhysicsObject::setVelocity(const glm::vec2& v)
{
    assert(mBody != nullptr);
    mBody->SetLinearVelocity({v.x * mScale, v.y * mScale});
}
