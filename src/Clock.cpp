#include "Clock.hpp"

#include <chrono>

auto Clock::getInstance() -> Clock&
{
    static Clock instance;
    return instance;
}

void Clock::start()
{
    mPrevious = getMillis();
    mLagPrevious = mPrevious;
}

void Clock::tick()
{
    mCurrent = getMillis();
    mElapsed = mCurrent - mPrevious;
    mPrevious = mCurrent;
    mLagCounter += mElapsed;
}

void Clock::lagTick()
{
    mLagCounter -= getInterval();
    mLagCurrent = getMillis();
    mLagElapsed = mLagCurrent - mLagPrevious;
    mLagPrevious = mLagCurrent;
}

auto Clock::isLagging() const -> bool
{
    return mLagCounter >= getInterval();
}

auto Clock::getMillis() const -> uint64_t
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch())
        .count();
}

auto Clock::getInterval() const -> uint64_t
{
    return 20;
}