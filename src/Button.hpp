#ifndef SUIKA_BUTTON_HPP
#define SUIKA_BUTTON_HPP

#include "AnimatedSprite.hpp"

#include <SDL2/SDL.h>

class Button
{
public:
    Button()
    {
        mSprite.getAnimPlayer().setShouldLoop(false);
    }
    ~Button() = default;

    void handleEvent(SDL_Event*);
    void update();
    auto getSprite() -> AnimatedSprite&;
    auto isClicked() -> bool;

private:
    AnimatedSprite mSprite;
    bool mClicked{false};
    bool mIsPressed{false};
};

#endif // SUIKA_BUTTON_HPP
