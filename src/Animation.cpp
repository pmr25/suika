#include "Animation.hpp"
#include "imemstream.hpp"

#include <cereal/archives/xml.hpp>

auto Animation::open(AssetFileHandle& handle) -> std::optional<AnimationError>
{
    const std::string content(handle.getData().begin(), handle.getData().end());
    imemstream stream(reinterpret_cast<const char*>(handle.getData().data()), handle.getData().size());
    AnimationFile data;

    try
    {
        cereal::XMLInputArchive archive(stream);
        archive(data);
    }
    catch (cereal::Exception& ex)
    {
        return {AnimationError::CannotOpenAnimation};
    }

    mFrames = data.frames;
    for (const auto& anim : data.animations)
    {
        assert(mAnimations.size() == mAnimationFPS.size());

        mAnimationLookup[anim.name] = static_cast<int>(mAnimations.size());
        mAnimations.push_back(anim.frameIndexes);
        mAnimationFPS.push_back(anim.fps);
    }

    return std::nullopt;
}

auto Animation::getAnimationID(const std::string& key) const -> int
{
    return mAnimationLookup.contains(key) ? mAnimationLookup.at(key) : -1;
}

auto Animation::getTickTimer(int animID) const -> TickTimer
{
    std::chrono::milliseconds dur(uint64_t(round(1000.0 / mAnimationFPS[animID])));
    return TickTimer(dur);
}

auto Animation::getFrameIndexes(int animID) const -> const std::vector<int>&
{
    assert(animID < static_cast<int>(mAnimations.size()));
    return mAnimations[animID];
}

auto Animation::getFrame(int frameID) const -> const SDL_Rect&
{
    assert(frameID < static_cast<int>(mFrames.size()));
    return mFrames[frameID];
}
