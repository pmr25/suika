#ifndef FONT_HPP
#define FONT_HPP

#include "Assets.hpp"

#include <SDL_ttf.h>
#include <memory>
#include <string>

class Font
{
public:
    Font(const std::string& fn)
    {
        open(fn);
    }
    ~Font()
    {
        destroy();
    }

    [[nodiscard]] auto getFont() const -> TTF_Font*;
    [[nodiscard]] auto getPtSize() const -> int;

    void open(const std::string&);
    void destroy();

private:
    TTF_Font* mFont{nullptr};
    std::unique_ptr<AssetFileHandle> mFontHandle;
    int mPtSize{48};
};

#endif // FONT_HPP
