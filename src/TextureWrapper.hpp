#ifndef TEXTUREWRAPPER_HPP
#define TEXTUREWRAPPER_HPP

#include "Texture.hpp"

class TextureWrapper final : public Texture
{
public:
    ~TextureWrapper() override
    {
        TextureWrapper::destroy();
    }

    void destroy() override;
    [[nodiscard]] auto getDimensions() const -> const glm::vec2& override;
    void setDimensions(const glm::vec2&) override;
    [[nodiscard]] auto getSDLTexture() const -> SDL_Texture* override;
    void setSDLTexture(SDL_Texture*, int, int);

private:
    glm::vec2 mDimensions{0.0F};
    SDL_Texture* mTexture{nullptr};
};

#endif // TEXTUREWRAPPER_HPP
