#ifndef SUIKA_LOGGER_HPP
#define SUIKA_LOGGER_HPP

namespace logger
{

auto init() -> bool;

}

#endif // SUIKA_LOGGER_HPP
