#ifndef SUIKA_WAV_HPP
#define SUIKA_WAV_HPP

#include <al.h>
#include <cstdint>
#include <string>
#include <vector>

enum class PCMFormat
{
    MONO8 = 1,
    STEREO8 = 2,
    MONO16 = 2,
    STEREO16 = 4,
};

struct RIFFHeader
{
    char riff[4];
    int32_t file_size;
    char file_type[4];
    char fmt[4];
    int32_t format_length;
    int16_t format_type;
    int16_t num_channels;
    int32_t sample_rate;
    int32_t bit_rate;
    int16_t sample_channel_bytes;
    int16_t bits_per_sample;
    char data[4];
    int32_t data_size;
};

class WAV
{
public:
    explicit WAV(const std::string& fn)
    {
        load(fn);
    }

    explicit WAV(std::istream& in)
    {
        load(in);
    }

    ~WAV() = default;

    void load(const std::string&);
    void load(std::istream&);
    [[nodiscard]] auto validateHeader() const -> bool;

    [[nodiscard]] auto getFormat() const -> PCMFormat;
    [[nodiscard]] auto getSize() const -> int32_t;
    [[nodiscard]] auto getRate() const -> int32_t;
    [[nodiscard]] auto getData() const -> const uint8_t*;

private:
    RIFFHeader mHeader;
    std::vector<uint8_t> mPCM;
};

#endif // SUIKA_WAV_HPP
