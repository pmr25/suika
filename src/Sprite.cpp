#include "Sprite.hpp"

auto Sprite::getPosition() const -> const glm::vec2&
{
    return mPosition;
}

auto Sprite::getCenter() const -> glm::vec2
{
    return getPosition() + 0.5F * getDimensions();
}

void Sprite::setPosition(glm::vec2 v)
{
    mPosition = v;
}

void Sprite::setCenter(glm::vec2 v)
{
    mPosition = v - 0.5F * getDimensions();
}

auto Sprite::getVelocity() const -> const glm::vec2&
{
    return mVelocity;
}

void Sprite::setVelocity(glm::vec2 v)
{
    mVelocity = v;
}

auto Sprite::getTheta() const -> float
{
    return mTheta;
}

void Sprite::setTheta(float t)
{
    mTheta = t;
}

auto Sprite::getOpacity() const -> uint8_t
{
    return mOpacity;
}

void Sprite::setOpacity(uint8_t val)
{
    mOpacity = val;
}

auto Sprite::getBBox() const -> glm::vec4
{
    return {mPosition.x, mPosition.y, mDimensions.x, mDimensions.y};
}

auto Sprite::getDimensions() const -> const glm::vec2&
{
    return mDimensions;
}

void Sprite::setDimensions(glm::vec2 d)
{
    mDimensions = d;
}

void Sprite::setTexture(const std::shared_ptr<Texture>& t)
{
    mTexture = t;
    setDimensions(glm::vec2(mTexture->getDimensions().x, mTexture->getDimensions().y));
    mFrame = glm::vec4(0, 0, mTexture->getDimensions().x, mTexture->getDimensions().y);
}

auto Sprite::getTexture() const -> const std::shared_ptr<Texture>&
{
    return mTexture;
}

void Sprite::setColorMod(glm::vec3 v)
{
    mColorMod = v;
}

auto Sprite::getColorMod() const -> const glm::vec3&
{
    return mColorMod;
}

void Sprite::update()
{
    if (!mIsVisible)
    {
        return;
    }
    mPosition += mVelocity;
}

void Sprite::setVisible(bool b)
{
    mIsVisible = b;
}

auto Sprite::isVisible() const -> bool
{
    return mIsVisible;
}
void Sprite::setFrame(const glm::vec4& vec)
{
    mFrame = vec;
}
auto Sprite::getFrame() const -> glm::vec4
{
    return mFrame;
}
