#ifndef CLOCK_HPP
#define CLOCK_HPP

#include <cstdint>

class Clock
{
public:
    ~Clock() = default;

    static auto getInstance() -> Clock&;

    void start();
    void tick();
    void lagTick();
    [[nodiscard]] auto getMillis() const -> uint64_t;
    [[nodiscard]] auto getInterval() const -> uint64_t;
    [[nodiscard]] auto isLagging() const -> bool;

private:
    Clock()
    {
        start();
    }

    uint64_t mCurrent{0};
    uint64_t mPrevious{0};
    uint64_t mElapsed{0};
    uint64_t mLagCurrent{0};
    uint64_t mLagCounter{0};
    uint64_t mLagPrevious{0};
    uint64_t mLagElapsed{0};
};

#endif /* CLOCK_HPP */