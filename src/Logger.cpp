#include "Logger.hpp"
#include "AppData.hpp"
#include "Clock.hpp"

#include <filesystem>
#include <iostream>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/spdlog.h>

auto logger::init() -> bool
{
    auto now{std::time(nullptr)};
    auto localNow{*std::localtime(&now)};
    std::stringstream filename;
    filename << std::put_time(&localNow, "%Y-%m-%d_%H_%M_%S") << ".log";

    try
    {
        std::vector<spdlog::sink_ptr> sinks;
        auto file_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(
            (std::filesystem::path(AppData::getAppDataDir()) / std::filesystem::path("logs") /
             std::filesystem::path(filename.str())));
        sinks.push_back(file_sink);
        sinks.push_back(std::make_shared<spdlog::sinks::ansicolor_stdout_sink_st>());
        auto logger = std::make_shared<spdlog::logger>("logger", begin(sinks), end(sinks));
#ifdef DEBUG
        logger->set_level(spdlog::level::debug);
        logger->flush_on(spdlog::level::debug);
#else
        logger->flush_on(spdlog::level::critical);
#endif /* DEBUG */
        spdlog::set_default_logger(logger);
    }
    catch (const spdlog::spdlog_ex& ex)
    {
        std::cout << ex.what() << std::endl;
        return false;
    }

    return true;
}
