#ifndef SUIKA_ANIMATIONPLAYER_HPP
#define SUIKA_ANIMATIONPLAYER_HPP

#include "TickTimer.hpp"

#include <SDL_rect.h>
#include <glm/glm.hpp>
#include <memory>
#include <optional>

class Animation;

enum class AnimationPlayerError
{
    AnimationNotFound,
    AnimationNotSet,
    AnimationAlreadySet,
};

class AnimationPlayer
{
public:
    auto setAnimationProvider(const std::shared_ptr<Animation>&) -> void;
    auto setCurrentAnimation(const std::string&) -> std::optional<AnimationPlayerError>;
    auto update() -> void;
    [[nodiscard]] auto getCurrentFrame() const -> SDL_Rect;
    [[nodiscard]] auto getCurrentFrameVector() const -> const glm::vec4;
    [[nodiscard]] auto isReady() const -> bool;
    [[nodiscard]] auto isRunning() const -> bool;
    auto setShouldLoop(bool) -> void;
    auto play() -> void;

private:
    int mAnimID{-1};
    int mPosition{0};
    bool mIsRunning{false};
    bool mShouldLoop{true};
    std::shared_ptr<Animation> mAnimation;
    TickTimer mTimer;
};

#endif // SUIKA_ANIMATIONPLAYER_HPP
