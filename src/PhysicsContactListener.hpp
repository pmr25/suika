#ifndef PHYSICSCONTACTLISTENER_HPP
#define PHYSICSCONTACTLISTENER_HPP

#include <box2d/box2d.h>

class PhysicsContactListener : public b2ContactListener
{
public:
    void BeginContact(b2Contact* contact) override;
    void EndContact(b2Contact* contact) override;
};

#endif /* PHYSICSCONTACTLISTENER_HPP */
