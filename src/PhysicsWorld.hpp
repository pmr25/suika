#ifndef PHYSICSWORLD_HPP
#define PHYSICSWORLD_HPP

#include "CollisionListener.hpp"
#include "PhysicsContactListener.hpp"
#include "PhysicsObject.hpp"

#include <box2d/box2d.h>
#include <glm/glm.hpp>

#include <memory>
#include <optional>
#include <unordered_map>

enum class PhysicsError
{
    NOTFOUND,
};

class PhysicsWorld
{
public:
    PhysicsWorld()
    {
        create();
    }
    static auto getInstance() -> PhysicsWorld&;

    void create();
    void step(float, int, int);

    auto createPhysicsObject(glm::vec4, float, bool, CollisionListener*) -> int;
    auto createPhysicsObject(float, bool) -> int;
    auto getPhysicsObject(int id) -> PhysicsObject*;
    auto deletePhysicsObject(int id) -> std::optional<PhysicsError>;
    auto getB2DWorld() const -> b2World*;
    auto getScale() const -> float;

private:
    b2Vec2 mGravity;
    std::unique_ptr<b2World> mWorld;
    std::unique_ptr<PhysicsContactListener> mContactListener;
    std::unordered_map<int, PhysicsObject> mObjects;
    float mInternalScale{0.1F};
};

#endif /* PHYSICSWORLD_HPP */
