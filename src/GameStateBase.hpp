#ifndef SUIKA_GAMESTATEBASE_HPP
#define SUIKA_GAMESTATEBASE_HPP

#include "GameState.hpp"

class GameStateBase : public GameState
{
public:
    [[nodiscard]] auto isReady() const -> bool override;
    void setReady(bool) override;
    [[nodiscard]] auto getCurrentPhase() const -> std::pair<GameStatePhase, std::string> override;
    void setPhase(GameStatePhase, bool) override;
    void setNextState(const std::string&) override;

private:
    bool mIsReady{false};
    std::string mNextState;
    GameStatePhase mCurrentPhase{GameStatePhase::MAIN};
};

#endif // SUIKA_GAMESTATEBASE_HPP
