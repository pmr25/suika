#include "EndState.hpp"
#include "Animation.hpp"
#include "AppData.hpp"
#include "Screen.hpp"
#include "TextureImage.hpp"

#include <memory>

void EndState::init()
{
    mFont = std::make_shared<Font>("fonts/ShantellSans-VariableFont_BNCE,INFM,SPAC,wght.ttf");
    mCurrentScoreText = std::make_unique<TextLine>(mFont);
    mCurrentScoreText->setPtSize(48);
    mCurrentScoreText->getSprite().setPosition({16, 0});

    mHighScoreText = std::make_unique<TextLine>(mFont);
    mHighScoreText->setPtSize(48);
    mHighScoreText->getSprite().setPosition({16, 100});

    auto animation{std::make_shared<Animation>()};
    auto animHandle{Assets::getInstance().getFile("animations/two_frame_button.xml")};
    animation->open(animHandle);
    auto continueButtonTex{std::make_shared<TextureImage>()};
    continueButtonTex->open(Assets::getInstance().getFile("textures/button_continue.png"), 128, 128);
    mContinueButton.getSprite().getSprite().setTexture(continueButtonTex);
    mContinueButton.getSprite().getSprite().setDimensions({128, 64});
    mContinueButton.getSprite().setAnimationProvider(animation);
    mContinueButton.getSprite().getSprite().setCenter(
        {Screen::getInstance().getWidth() / 2, Screen::getInstance().getHeight() * 3 / 4});

    GameStateBase::setReady(true);
}

void EndState::handleEvent(SDL_Event* event)
{
    const auto [currentPhase, nextState] = GameStateBase::getCurrentPhase();
    if (currentPhase == GameStatePhase::MAIN)
    {
        mContinueButton.handleEvent(event);
    }
}

void EndState::update()
{
    const auto [currentPhase, nextState] = GameStateBase::getCurrentPhase();
    if (currentPhase == GameStatePhase::ENTER)
    {
        const auto& data{AppData::getInstance()};
        const auto lastScore{data.getLastScore()};
        const auto highScore{data.getHighScore()};
        if (data.hasNewHighScore())
        {
            mCurrentScoreText->setText("New high score!    " + std::to_string(lastScore));
            mHighScoreText->setText("");
        }
        else
        {
            mCurrentScoreText->setText("Current Score: " + std::to_string(lastScore));
            mHighScoreText->setText("High Score: " + std::to_string(highScore));
        }
        setPhase(GameStatePhase::MAIN, true);
    }
    else if (currentPhase == GameStatePhase::MAIN)
    {
        if (mContinueButton.isClicked())
        {
            GameStateBase::setNextState("title");
            GameStateBase::setPhase(GameStatePhase::IDLE, false);
        }

        mBackground.update();
        mHighScoreText->update();
        mCurrentScoreText->update();
        mContinueButton.update();
    }
}

void EndState::draw()
{
    const auto& screen{Screen::getInstance()};
    const auto [currentPhase, nextState] = GameStateBase::getCurrentPhase();

    for (const auto& sprite : mBackground.getSprites())
    {
        screen.draw(sprite);
    }

    if (currentPhase == GameStatePhase::MAIN)
    {
        screen.draw(mHighScoreText->getSprite());
        screen.draw(mCurrentScoreText->getSprite());
        screen.draw(mContinueButton.getSprite().getSprite());
    }
}
