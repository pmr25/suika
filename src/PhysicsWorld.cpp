#include "PhysicsWorld.hpp"

static int physObjIdCounter = 0;

auto PhysicsWorld::getInstance() -> PhysicsWorld&
{
    static PhysicsWorld instance;
    return instance;
}

void PhysicsWorld::create()
{
    mGravity = b2Vec2(0.0F, 30.0F);
    mWorld = std::make_unique<b2World>(mGravity);
    mContactListener = std::make_unique<PhysicsContactListener>();
    mWorld->SetContactListener(mContactListener.get());
}

void PhysicsWorld::step(float timeStep, int velocityIters, int positionIters)
{
    mWorld->Step(timeStep, velocityIters, positionIters);
}

auto PhysicsWorld::createPhysicsObject(glm::vec4 box, float density, bool isDynamic, CollisionListener* listener) -> int
{
    PhysicsObject obj(getB2DWorld(), isDynamic, listener);
    obj.createRect(box.x, box.y, box.z, box.w);

    const auto id{physObjIdCounter++};
    mObjects.emplace(std::make_pair(id, std::move(obj)));

    return id;
}

auto PhysicsWorld::createPhysicsObject(float density, bool isDynamic) -> int
{
    PhysicsObject obj(getB2DWorld(), isDynamic, nullptr);

    const auto id{physObjIdCounter++};
    mObjects.emplace(std::make_pair(id, std::move(obj)));

    return id;
}

auto PhysicsWorld::getPhysicsObject(int id) -> PhysicsObject*
{
    if (mObjects.find(id) == mObjects.end())
    {
        return nullptr;
    }

    return &mObjects.at(id);
}

auto PhysicsWorld::deletePhysicsObject(int id) -> std::optional<PhysicsError>
{
    if (mObjects.find(id) == mObjects.end())
    {
        return std::optional<PhysicsError>(PhysicsError::NOTFOUND);
    }

    mObjects.at(id).destroyBody(mWorld.get());

    return std::nullopt;
}

auto PhysicsWorld::getB2DWorld() const -> b2World*
{
    return mWorld.get();
}

auto PhysicsWorld::getScale() const -> float
{
    return mInternalScale;
}
