#include "Font.hpp"
#include "Screen.hpp"

#include <cmath>

auto Font::getFont() const -> TTF_Font*
{
    return mFont;
}

auto Font::getPtSize() const -> int
{
    return static_cast<int>(round(Screen::getInstance().screenToDevice(mPtSize)));
}

void Font::open(const std::string& fn)
{
    mFontHandle = std::make_unique<AssetFileHandle>(Assets::getInstance().getFile(fn));
    mFont = TTF_OpenFontRW(mFontHandle->getOps(), 0, getPtSize());
    TTF_SetFontStyle(mFont, TTF_STYLE_NORMAL);
}

void Font::destroy()
{
    if (mFont != nullptr)
    {
        TTF_CloseFont(mFont);
    }
}
