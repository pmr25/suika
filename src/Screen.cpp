#include "Screen.hpp"
#include "Assets.hpp"
#include "GameLoop.hpp"
#include "SfxPlayer.hpp"

#include <SDL_image.h>
#include <SDL_ttf.h>
#include <iostream>
#include <spdlog/spdlog.h>
#include <vips/vips8>

auto Screen::getInstance() -> Screen&
{
    static Screen instance;
    return instance;
}

void Screen::create(uint32_t w, uint32_t h)
{
    mWidth = w;
    mHeight = h;

    SDL_DisplayMode dm;
    SDL_GetCurrentDisplayMode(0, &dm);
    mScale = round(double(dm.h) / 1080.0);

    mWindow = SDL_CreateWindow("Suika", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, mWidth * mScale,
                               mHeight * mScale, 0);
    if (mWindow == nullptr)
    {
        return;
    }

    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
    mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_PRESENTVSYNC);
    if (mRenderer == nullptr)
    {
        return;
    }
    SDL_RenderSetLogicalSize(getRenderer(), mWidth * mScale, mHeight * mScale);

    mWindowIcon = IMG_Load_RW(Assets::getInstance().getFile("metadata/suika.png").getOps(), 0);
    if (mWindowIcon == nullptr)
    {
        return;
    }

    SDL_SetWindowIcon(mWindow, mWindowIcon);
}

void Screen::draw(const Sprite& sprite) const
{
    if (!sprite.isVisible())
    {
        return;
    }

    SDL_Rect src;
    SDL_Rect dst;
    const auto& texture{sprite.getTexture()};
    const auto& frame{sprite.getFrame()};

    src.x = frame.x;
    src.y = frame.y;
    src.w = frame.z;
    src.h = frame.w;
    const int srcArea{src.w * src.h};
    const SDL_Rect* srcPtr = srcArea > 0 ? &src : nullptr;

    dst.x = sprite.getPosition().x * mScale;
    dst.y = sprite.getPosition().y * mScale;
    dst.w = sprite.getDimensions().x * mScale;
    dst.h = sprite.getDimensions().y * mScale;

    const auto& colorMod{sprite.getColorMod()};
    SDL_SetTextureColorMod(texture->getSDLTexture(), uint8_t(roundf(colorMod.r * 255)),
                           uint8_t(roundf(colorMod.g * 255)), uint8_t(roundf(colorMod.b * 255)));

    const auto alphaErr{SDL_SetTextureAlphaMod(texture->getSDLTexture(), sprite.getOpacity())};
    if (alphaErr < 0)
    {
        // TODO handle err
        return;
    }

    const auto renderErr{SDL_RenderCopyEx(mRenderer, texture->getSDLTexture(), srcPtr, &dst,
                                          sprite.getTheta() * 180.0F / M_PI, nullptr, SDL_FLIP_NONE)};
    if (renderErr < 0)
    {
        // TODO handle err
        return;
    }
}

void Screen::draw(SDL_Texture* tex, SDL_Rect* src, SDL_Rect* dst) const
{
    SDL_RenderCopy(mRenderer, tex, src, dst);
}

auto Screen::getRenderer() const -> SDL_Renderer*
{
    return mRenderer;
}

void Screen::clear() const
{
    SDL_RenderClear(mRenderer);
}

void Screen::flip() const
{
    SDL_RenderPresent(mRenderer);
}

auto Screen::getWidth() const -> uint32_t
{
    return mWidth;
}

auto Screen::getHeight() const -> uint32_t
{
    return mHeight;
}

auto Screen::intersects(glm::vec4 bbox) const -> bool
{
    return bbox.x + bbox.z >= 0 && bbox.x <= mWidth && bbox.y + bbox.w >= 0 && bbox.y <= mHeight;
}

auto Screen::deviceToScreen(double val) const -> double
{
    return val / mScale;
}

auto Screen::screenToDevice(double val) const -> double
{
    return val * mScale;
}

auto Screen::init() -> void
{
    int err = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_AUDIO);
    if (err != 0)
    {
        spdlog::critical("SDL init error with reason \"{}\"", SDL_GetError());
        GameLoop::exit();
    }

    err = IMG_Init(IMG_INIT_PNG);
    if (err != IMG_INIT_PNG)
    {
        spdlog::critical("SDL_image init error with reason: \"{}\"", IMG_GetError());
        GameLoop::exit();
    }

    err = TTF_Init();
    if (err != 0)
    {
        spdlog::critical("SDL_ttf init error with reason: \"{}\"", TTF_GetError());
        GameLoop::exit();
    }

    if (VIPS_INIT("suika"))
    {
        spdlog::critical("vips init error");
        GameLoop::exit();
    }

    mALDevice = alcOpenDevice(nullptr);
    if (!mALDevice)
    {
        spdlog::warn("cannot open audio device");
        mAudioEnabled = false;
    }

    mALContext = alcCreateContext(mALDevice, nullptr);
    if (!mALContext)
    {
        spdlog::warn("openal error");
        mAudioEnabled = false;
    }

    if (!alcMakeContextCurrent(mALContext))
    {
        spdlog::warn("openal error");
        mAudioEnabled = false;
    }
}

auto Screen::destroy() -> void
{
    SfxPlayer::getInstance().destroy();
    if (mWindowIcon)
    {
        SDL_FreeSurface(mWindowIcon);
    }

    SDL_DestroyRenderer(mRenderer);
    SDL_DestroyWindow(mWindow);

    alcMakeContextCurrent(nullptr);

    if (mALContext)
    {
        alcDestroyContext(mALContext);
    }

    if (mALDevice)
    {
        alcCloseDevice(mALDevice);
    }

    vips_shutdown();
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

auto Screen::isAudioEnabled() const -> bool
{
    return mAudioEnabled;
}
