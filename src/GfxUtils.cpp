#include "GfxUtils.hpp"

#include <vips/vips8>

namespace utils::gfx
{

std::vector<uint8_t> shrinkImage(const std::vector<uint8_t>& raster, uint32_t width, uint32_t height, uint32_t channels,
                                 uint32_t newW, uint32_t newH)
{
    vips::VImage img = vips::VImage::new_from_memory((void*)(&raster[0]), width * height * channels, width, height,
                                                     channels, VIPS_FORMAT_UCHAR);

    const auto hshrink{double(width) / double(newW)};
    const auto vshrink{double(height) / double(newH)};
    vips::VImage reducedImg = img.reduce(hshrink, vshrink);

    const auto imgSize{reducedImg.width() * reducedImg.height() * reducedImg.bands()};
    const auto ptr{(uint8_t*)(reducedImg.data())};
    std::vector<uint8_t> shrunken(ptr, ptr + imgSize);

    return shrunken;
}

} // namespace utils::gfx
