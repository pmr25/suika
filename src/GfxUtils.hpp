#ifndef SUIKA_GFXUTILS_HPP
#define SUIKA_GFXUTILS_HPP

#include <cstdint>
#include <vector>

namespace utils::gfx
{
std::vector<uint8_t> shrinkImage(const std::vector<uint8_t>& raster, uint32_t width, uint32_t height, uint32_t channels,
                                 uint32_t newW, uint32_t newH);
}

#endif // SUIKA_GFXUTILS_HPP
