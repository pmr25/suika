#include "Button.hpp"

void Button::handleEvent(SDL_Event* event)
{
    if (event->type == SDL_MOUSEBUTTONDOWN)
    {
        if (event->button.button != SDL_BUTTON_LEFT)
        {
            return;
        }

        const glm::vec4 bbox{mSprite.getSprite().getBBox()};
        const bool intersect{event->button.x >= bbox[0] && event->button.x <= bbox[0] + bbox[2] &&
                             event->button.y >= bbox[1] && event->button.y <= bbox[1] + bbox[3]};

        mIsPressed = intersect;

        if (intersect)
        {
            mSprite.setCurrentAnimation("forward");
        }
    }
    else if (event->type == SDL_MOUSEBUTTONUP)
    {
        if (event->button.button != SDL_BUTTON_LEFT)
        {
            return;
        }

        const glm::vec4 bbox{mSprite.getSprite().getBBox()};
        const bool intersect{event->button.x >= bbox[0] && event->button.x <= bbox[0] + bbox[2] &&
                             event->button.y >= bbox[1] && event->button.y <= bbox[1] + bbox[3]};

        mClicked = intersect && mIsPressed;

        if (intersect || mIsPressed)
        {
            mSprite.setCurrentAnimation("backward");
        }
    }
}

void Button::update()
{
    mSprite.update();
}

auto Button::getSprite() -> AnimatedSprite&
{
    return mSprite;
}
auto Button::isClicked() -> bool
{
    const bool snapshot{mClicked};
    mClicked = false;
    return snapshot;
}
