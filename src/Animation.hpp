#ifndef ANIMATION_HPP
#define ANIMATION_HPP

#include "Assets.hpp"

#include "TickTimer.hpp"

#include <SDL2/SDL.h>

#include <cereal/types/vector.hpp>
#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

template <class Archive>
inline void serialize(Archive& ar, SDL_Rect& rect)
{
    ar(rect.x, rect.y, rect.w, rect.h);
}

enum class AnimationError
{
    CannotOpenAnimation,
};

class AnimationData
{
public:
    std::string name;
    std::vector<int> frameIndexes;
    double fps;

    template <class Archive>
    void serialize(Archive& ar)
    {
        ar(name, fps, frameIndexes);
    }
};

class AnimationFile
{
public:
    std::vector<SDL_Rect> frames;
    std::vector<AnimationData> animations;

    template <class Archive>
    void serialize(Archive& ar)
    {
        ar(frames, animations);
    }
};

class Animation
{
public:
    Animation() = default;
    ~Animation() = default;

    auto open(AssetFileHandle&) -> std::optional<AnimationError>;
    [[nodiscard]] auto getAnimationID(const std::string& key) const -> int;
    [[nodiscard]] auto getTickTimer(int animID) const -> TickTimer;
    [[nodiscard]] auto getFrameIndexes(int animID) const -> const std::vector<int>&;
    [[nodiscard]] auto getFrame(int frameID) const -> const SDL_Rect&;

private:
    std::unordered_map<std::string, int> mAnimationLookup;
    std::vector<std::vector<int>> mAnimations;
    std::vector<double> mAnimationFPS;
    std::vector<SDL_Rect> mFrames;
};

#endif // ANIMATION_HPP
