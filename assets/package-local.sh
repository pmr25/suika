#!/bin/bash

declare -a names=($(cd inkscape; ls *.svg | sed -e 's/\.svg$//'))
for name in "${names[@]}"
do
    inkscape -w 1024 "./inkscape/${name}.svg" -o "./textures/${name}.png"
done

tar -cf assets.tar animations/ fonts/ gamedata/ sound/ textures/ metadata/suika.png
gcc -c assets.S -o assets.o
ar -rc libassets.a assets.o
rm -f assets.o
rm -f assets.tar