#!/bin/bash

wget https://suika-assets.s3.us-east-2.amazonaws.com/assets.tar.gz
gunzip assets.tar.gz
gcc -c assets.S -o assets.o
ar -rc libassets.a assets.o
rm -f assets.o
rm -f assets.tar
