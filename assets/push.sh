#!/bin/bash

tar -cf assets.tar animations/ fonts/ gamedata/ sound/ textures/ metadata/suika.png
gzip assets.tar
aws s3 cp assets.tar.gz s3://suika-assets --acl public-read
rm -f assets.tar.gz
