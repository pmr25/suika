use chrono::Utc;
use flate2::write::GzEncoder;
use flate2::Compression;
use std::io::{self, BufRead};
use std::path::PathBuf;
use std::{fs, path};

struct LogFile {
    num_info: u32,
    num_warning: u32,
    num_critical: u32,
    num_lines: u32,
}

impl LogFile {
    fn should_delete(&self) -> bool {
        self.num_critical == 0 || self.num_lines == 0
    }
}

fn print_help_page() {
    println!("usage: housekeeper <logs directory>");
}

fn read_lines<P: AsRef<path::Path>>(filename: P) -> io::Result<io::Lines<io::BufReader<fs::File>>> {
    let file = fs::File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn parse_log(filename: path::PathBuf) -> io::Result<LogFile> {
    let mut logfile = LogFile {
        num_info: 0,
        num_critical: 0,
        num_warning: 0,
        num_lines: 0,
    };

    if let Ok(lines) = read_lines(filename) {
        for line in lines.flatten() {
            let counts = parse_line(line);
            logfile.num_info += counts.0;
            logfile.num_warning += counts.1;
            logfile.num_critical += counts.2;
            logfile.num_lines += 1;
        }
    }

    Ok(logfile)
}

fn parse_line(line: String) -> (u32, u32, u32) {
    (
        if line.contains("[info]") { 1 } else { 0 },
        if line.contains("[warning]") { 1 } else { 0 },
        if line.contains("[critical]") { 1 } else { 0 },
    )
}

fn main() -> io::Result<()> {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        println!("error: invalid arguments");
        print_help_page();
        return Ok(());
    }

    let work_dir = &args[1];
    let mut to_delete: Vec<PathBuf> = Vec::new();
    let mut to_pack: Vec<PathBuf> = Vec::new();
    for entry in fs::read_dir(work_dir)? {
        let path = entry?.path();
        if path.is_dir() || !str::ends_with(path.file_name().unwrap().to_str().unwrap(), ".log") {
            continue;
        }

        let stats = parse_log(path.clone())?;
        if stats.should_delete() {
            to_delete.push(path);
        } else {
            to_pack.push(path);
        }
    }

    for filename in to_delete {
        fs::remove_file(filename)?
    }

    if to_pack.len() > 0
    {
        let now = Utc::now();
        let tar_archive = fs::File::create(format!("{}.tar.gz", now.format("%Y-%m-%d_%H_%M_%S")))?;
        let encoder = GzEncoder::new(tar_archive, Compression::default());
        let mut builder = tar::Builder::new(encoder);
        for filename in to_pack {
            let mut fp = fs::File::open(filename.as_path())?;
            let local_path = path::Path::new(filename.as_path().file_name().unwrap());
            builder.append_file(local_path, &mut fp)?;
        }
    }

    Ok(())
}
