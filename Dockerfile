FROM ubuntu:20.04 AS base

ARG DEBIAN_FRONTEND=noninteractive
RUN apt update ; apt install -y build-essential python3-venv pipx ninja-build wget curl libfuse2 file software-properties-common git zsync
RUN pipx install meson
ENV PATH "/root/.local/bin:${PATH}"
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"

FROM base AS deps

ARG DEBIAN_FRONTEND=noninteractive
RUN apt update ; apt install -y libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libarchive-dev libglm-dev libvips-dev libopenal-dev libcereal-dev

FROM deps AS tools

RUN wget -q https://github.com/AppImage/AppImageKit/releases/download/13/appimagetool-x86_64.AppImage -O /usr/bin/appimagetool ; chmod +x /usr/bin/appimagetool
RUN wget -q https://github.com/linuxdeploy/linuxdeploy/releases/download/1-alpha-20231206-1/linuxdeploy-x86_64.AppImage -O /usr/bin/linuxdeploy ; chmod +x /usr/bin/linuxdeploy
RUN sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d -b /usr/bin

FROM tools AS image
