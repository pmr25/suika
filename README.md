# suika

## Description
Game written in C++

## Usage
Install [task](https://taskfile.dev/).
See `task --list-all` for a list of operations to run, including building and packaging.

## Visuals
![title](https://gitlab.com/pmr25/suika/-/raw/main/docs/images/title.png)
![game](https://gitlab.com/pmr25/suika/-/raw/main/docs/images/game.png)
![end](https://gitlab.com/pmr25/suika/-/raw/main/docs/images/end.png)
